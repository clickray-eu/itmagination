$(document).ready(function () {
    mobileMenuInit();
    manageHeaderSearch();
})

function mobileMenuInit() {
    $('.block--menu .hs-menu-wrapper > ul').slicknav({
        label: '',
        appendTo: '.block--menu',
        slideDirection: 'right',
        closedSymbol: '<i class="fa fa-angle-down" aria-hidden="true"></i>',
        openedSymbol: '<i class="fa fa-angle-up" aria-hidden="true"></i>',
        init: () => {
            $('.slicknav_nav').addClass('preventDefault').addClass("slicknav_hidden");
        },
        beforeOpen: (trigger) => {
            $('.slicknav_nav').removeClass('preventDefault');
        }
    });
}

function manageHeaderSearch() {
    $('.block--search #search-btn').on('click', (e) => {
        $(e.target).closest('.input-wrapper').find('.search-popup').fadeIn(500);
        $('body').css('overflow', 'hidden');
    })
    $('.block--search .search-popup').on('click', (e) => {
        $(e.target).closest('.input-wrapper').find('.search-popup').fadeOut(500);
        e.stopPropagation();
        $('body').css('overflow', '');
    })
    $('.block--search .popup-inner').on('click', (e) => {
        e.stopPropagation();
    })
}