$(document).ready(function() {


    $('.home-slider-wrapper > span').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false
    });

    $('.home .brand-slider > span').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        responsive: [
		    {
		      breakpoint: 450,
		      settings: {
		        slidesToShow: 1
		      }
		    }
		]
    });


})