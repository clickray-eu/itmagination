$(document).ready(function() {


    $('.typ-slider-wrapper > span').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        adaptiveHeight: true
    });

})