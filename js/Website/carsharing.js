var isSlick = '';
var check = '';
$(document).ready(function(){

// CUSTOMERS SLIDER 

 $('.carsharing .brands-slider-wrapper > span').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        autoplay: true,
        responsive: [
		    {
		      breakpoint: 450,
		      settings: {
		        slidesToShow: 1
		      }
		    }
		]
    });


// BLOG SLIDER 


if( $(window).innerWidth() <= 768 ){
	$('.carsharing .latest-post-from-blog').slick({
	        infinite: true,
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        dots: true,
	        arrows: false
	    });



		isSlick = 1;

	}
	else{
		isSlick = 0;
		check = 1;
	}
})







$(window).resize(function() {
	

if( $(window).innerWidth() > 767 && check == 0  && isSlick == 1){
	
		$('.carsharing .latest-post-from-blog').slick('unslick');
		check = 1;
		// console.log(check );
	}
if( $(window).innerWidth() < 767 && check == 1 ){
		$('.carsharing .latest-post-from-blog').slick({
	        infinite: true,
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        dots: true,
	        arrows: false
	    });
	    check = 0;
	   isSlick = 1
	    // console.log(check);
	}
	

});