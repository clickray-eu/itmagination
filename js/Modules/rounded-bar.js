$(document).ready(function () {
    if ($('.rounded-bar--single').length > 0) {
        const progressBars = $('.rounded-bar--single');
        for (let i = 0; i < progressBars.length; i++) {
            let currentEl = progressBars.eq(i).find('.rounded-bar__svg')
            let currentPercentage = makePercentageVal(progressBars.eq(i).find('.rounded-bar__percentage').text());
            let bar = new ProgressBar.Circle(currentEl[0], {
                strokeWidth: 8,
                easing: 'linear',
                duration: 700,
                color: '#0098da',
                trailColor: '#bfc4c7',
                trailWidth: 8,
                svgStyle: null
            });
            bar.path.style.strokeLinecap = 'round';
            progressBars.eq(i).data("bars_done", false);
            $(window).on('scroll', function () {
                let windowHeight = $(window).height();
                let windowScrollTop = $(window).scrollTop();
                if (progressBars.eq(i).data("bars_done") == false) {
                    if (windowHeight + windowScrollTop > progressBars.eq(i).offset().top + 300) {
                        progressBars.eq(i).data("bars_done", true);
                        bar.animate(currentPercentage);
                    }
                }
            });
        }
        $(window).scroll();
    }

})


function makePercentageVal(val) {
    val = val.slice(0, -1).trim();
    return val / 100;
}

// const ua = navigator.userAgent.toLowerCase();
// if (/msie/.test(ua) || /rv:[\d.]+\) like gecko/.test(ua)) {
//     $('body').addClass('ms-browser');
//     // var pBars = document.querySelectorAll('.rounded-bar--single svg');
//     // for (var i = 0; i < pBars.length; i++) {
//     //     pBars[i].setAttributeNS(null, 'stroke-width', '6');
//     // }
// } 
if(/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) ||/Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1 ){
    // console.log('ms browser');
    $('body').addClass('ms-browser');
}