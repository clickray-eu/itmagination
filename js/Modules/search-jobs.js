var specsObject = undefined;
var citiesObject = {
    Łódź: 'lodz',
    Cracow: 'cracow',
    Katowice: 'katowice',
    Płock: 'plock',
    Wrocław: 'wroclaw',
    Gdynia: 'gdynia',
    Warsaw: 'warsaw'
}
$(document).ready(function () {
    let citiesArray = [];
    let specArray = [];
    suggestedCategories();
if ($('input#offers-city').length > 0) {
    const citiesSelect = new Awesomplete('input#offers-city', {
        list: prepareJobSelects(citiesArray, '.job-offer--single__city'),
        minChars: 0,
        replace: function (text) {
            this.input.value = text;
            $(this.input).attr("data-value", citiesObject[text.value] || text.value );
        }
    });
    const specSelect = new Awesomplete('input#offers-specialization', {
        list: prepareSpec(specArray, '.job-offer--single__spec'),
        minChars: 0,
        replace: function (text) {
            this.input.value = text;
            $(this.input).attr("data-value", specsObject[text.value] || text.value);
        }
    });
    Awesomplete.$(".offers-city-button").addEventListener("click", function () {
        managePopup(citiesSelect)
    });
    Awesomplete.$(".offers-specialization-button").addEventListener("click", function () {
        managePopup(specSelect)
    });
    $('.job-offers--result').isotope();
    isotopeFiltering();
}
    

})

function isotopeFiltering() {
    let req = 0;

    $('.search-bar--wrapper .input-hybrid ul').on('click', function () {
        $(this).closest('.awesomplete').find('input').trigger('change');
    })
    $('.suggested-categories li').on('click', function () {
        $('#search-pos').trigger('change');
    })
    $('.search-bar--wrapper input').on('change', function () {
        let test = [];
        $("#search-pos").val().length > 0 == true ? console.log('tak') : console.log('nie');
        if ($("#search-pos").val().length > 0) {
            req++;
            test.push("#search-pos");

        }
        if ($("#offers-city").val().length > 0) {
            req++;
            test.push("#offers-city");

        }
        if ($("#offers-specialization").val().length > 0) {
            req++;
            test.push("#offers-specialization");
        }
        $('.job-offers--result').isotope({
            filter: function () {
                if (req == 0) return true;
                if ($(this).attr("href") == "#") return false;
                var local_req = req;

                for (var i = 0; i < test.length; i++) {
                    var words = "";
                    if ($(test[i]).attr("data-value") == undefined)
                        words = $(test[i]).val().split(',');
                    else
                        words = $(test[i]).attr("data-value").split(',');

                    for (var j = 0; j < words.length; j++) {
                        var data_attr = test[i].replace("#", '');
                        if ($(this).data(data_attr).search(words[j].trim().toLowerCase()) > -1) {
                            local_req--;
                            break;
                        }
                    }
                }
                if (local_req == 0) return true;
                else return false;
            }
        });
        req = 0;
    })
}


function suggestedCategories() {
    $('.suggested-categories li').on('click', (e) => {
        $('#search-pos').val($(e.currentTarget).text());
    })
}
function prepareJobSelects(arr, target) {
    const cities = document.querySelectorAll(target);
    for (let index = 0; index < cities.length; index++) {
        if (arr.indexOf(cities[index].textContent) > -1) {
            continue;
        } else {
            arr.push(cities[index].textContent);
        }
    }
    return arr;
}
function prepareSpec(arr, target) {
    const cities = document.querySelectorAll(target);
    for (let index = 0; index < cities.length; index++) {
        let currentLabel = findSpecLabel(cities[index].textContent);
        if (arr.indexOf(currentLabel) > -1) {
            continue;
        } else {
            arr.push(currentLabel);
        }
    }
    return arr;
}

function managePopup(t) {
    0 === t.ul.childNodes.length ? (t.minChars = 0,
        t.evaluate()) : t.ul.hasAttribute("hidden") ? t.open() : t.close()
}
function findSpecLabel(label) {

    const specs = [{
        value: "software",
        label: "Software Development"
    }, {
        value: "data_management",
        label: "Business Intelligence & Data Management"
    }, {
        value: "data_science",
        label: "Data Science"
    }, {
        value: "management",
        label: "Management & Consulting"
    }, {
        value: "analysis",
        label: "Analysis"
    }, {
        value: "quality",
        label: "Quality Assurance"
    }, {
        value: "sales",
        label: "Sales & Business Development"
    }, {
        value: "it_supp_infra",
        label: "IT Support & Infrastructure"
    }, {
        value: "marketing",
        label: "PR & Marketing"
    }, {
        value: "hr_recruitment",
        label: "HR & Recruitment"
    }];
    if (specsObject == undefined) {
        specsObject = {};
        for (let i = 0; i < specs.length; i++) {
            specsObject[specs[i].label] = specs[i].value;
        }
    }
    for (let i = 0; i < specs.length; i++) {
        if (specs[i].value == label) {
            return specs[i].label;
        }
    }
}