$('.brands-slider > span').slick({
  autoplay: true,
  rows: 2,
  slidesPerRow: 3,
  autoplaySpeed: 5000,
  dots: true,
  arrows: false,
  responsive: [
    {
      breakpoint: 450,
      settings: {
        slidesPerRow: 2
      }
    },
    {
      breakpoint: 350,
      settings: {
        slidesPerRow: 1
      }
    }
  ]
})
$('.brands-slider-single > span').slick({
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 2,
  slidesToScroll: 2,
  dots: true,
  arrows: false
})