$('.single-member__toggle-desc').on('click', function(){
    const parentWrapper = $(this).closest('.single-member');
    const descToggle = parentWrapper.find('.single-member__description--toggle');
    if (parentWrapper.hasClass('open')){
        parentWrapper.removeClass('open');
        descToggle.slideUp();
    } else {
        parentWrapper.addClass('open');
        descToggle.slideDown();
    }
});