// Functions for dropdown
function select(el, currentElement) {
    $(currentElement).each(function () {
        $(this).find("select").each(function () {
            var parent = $(this).parent();
            parent.addClass('dropdown_select');
            parent.append('<div class="dropdown-header"><span>-Select-</span></div>');
            parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
            $(this).find('option').each(function () {
                if ($(this).val() != "") {
                    parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).val() + '</li>')
                }
            })
        })
    });
    $(currentElement).find('.dropdown_select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });

}

// Input type File 

function customInputFile() {

    $('input[type=file]').parent().parent().find('label:first-child').addClass("file-name");
    $('input[type=file]').addClass("inputfile");
    $('input[type=file]').parent().parent().addClass("inputfile-fake");

    var inputs = $('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {

        var label = document.querySelectorAll('.file-name')[0];

        input.addEventListener('change', function (e) {
            var fileName = e.target.value.split('\\').pop();

            if (fileName)
                label.querySelector('.file-name span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;

        });
    });

}

function addTrigger(i, e) {
    const triggerButton = $('<div class="show-more">Show More</div>');
    triggerButton.on('click', function () {
        let currentLi = $(this).closest('li');
        if (currentLi.hasClass('fixed-height')) {
            currentLi.removeClass('fixed-height')
            $(this).text('Show Less');
        } else {
            currentLi.addClass('fixed-height');
            $(this).text('Show More');
        }
    })
    $(e).find('.hs-form-booleancheckbox').each(function () {
        if ($(this).height() > 80) {
            $(this).addClass('fixed-height trigger');
            $(this).append(triggerButton.clone(true));
        }
    });
}
function checkForSelectChange(i,e){
    $(e).each(function () {
        $(this).find(".dropdown-list").each(function () {
            $(this).on('click', function(){
                $(this).closest('.dropdown_select').find('.dropdown-header').addClass('selected');
            })
        })
    });
}

waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", customInputFile)
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", select);
waitForLoad(".job-offers", "form", checkForSelectChange);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", addTrigger);


