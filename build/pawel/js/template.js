"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// file: _autocomplete.js
/**
 * Simple, lightweight, usable local autocomplete library for modern browsers
 * Because there weren’t enough autocomplete scripts in the world? Because I’m completely insane and have NIH syndrome? Probably both. :P
 * @author Lea Verou http://leaverou.github.io/awesomplete
 * MIT license
 */
/* jshint ignore:start */
(function () {

    var _ = function _(input, o) {
        var me = this;

        // Keep track of number of instances for unique IDs
        Awesomplete.count = (Awesomplete.count || 0) + 1;
        this.count = Awesomplete.count;

        // Setup

        this.isOpened = false;

        this.input = $(input);
        this.input.setAttribute("autocomplete", "off");
        this.input.setAttribute("aria-owns", "awesomplete_list_" + this.count);
        this.input.setAttribute("role", "combobox");

        o = o || {};

        configure(this, {
            minChars: 2,
            maxItems: 10,
            autoFirst: false,
            data: _.DATA,
            filter: _.FILTER_CONTAINS,
            sort: o.sort === false ? false : _.SORT_BYLENGTH,
            item: _.ITEM,
            replace: _.REPLACE
        }, o);

        this.index = -1;

        // Create necessary elements

        this.container = $.create("div", {
            className: "awesomplete",
            around: input
        });

        this.ul = $.create("ul", {
            hidden: "hidden",
            role: "listbox",
            id: "awesomplete_list_" + this.count,
            inside: this.container
        });

        this.status = $.create("span", {
            className: "visually-hidden",
            role: "status",
            "aria-live": "assertive",
            "aria-atomic": true,
            inside: this.container,
            textContent: this.minChars != 0 ? "Type " + this.minChars + " or more characters for results." : "Begin typing for results."
        });

        // Bind events

        this._events = {
            input: {
                "input": this.evaluate.bind(this),
                "blur": this.close.bind(this, { reason: "blur" }),
                "keydown": function keydown(evt) {
                    var c = evt.keyCode;

                    // If the dropdown `ul` is in view, then act on keydown for the following keys:
                    // Enter / Esc / Up / Down
                    if (me.opened) {
                        if (c === 13 && me.selected) {
                            // Enter
                            evt.preventDefault();
                            me.select();
                        } else if (c === 27) {
                            // Esc
                            me.close({ reason: "esc" });
                        } else if (c === 38 || c === 40) {
                            // Down/Up arrow
                            evt.preventDefault();
                            me[c === 38 ? "previous" : "next"]();
                        }
                    }
                }
            },
            form: {
                "submit": this.close.bind(this, { reason: "submit" })
            },
            ul: {
                "mousedown": function mousedown(evt) {
                    var li = evt.target;

                    if (li !== this) {

                        while (li && !/li/i.test(li.nodeName)) {
                            li = li.parentNode;
                        }

                        if (li && evt.button === 0) {
                            // Only select on left click
                            evt.preventDefault();
                            me.select(li, evt.target);
                        }
                    }
                }
            }
        };

        $.bind(this.input, this._events.input);
        $.bind(this.input.form, this._events.form);
        $.bind(this.ul, this._events.ul);

        if (this.input.hasAttribute("list")) {
            this.list = "#" + this.input.getAttribute("list");
            this.input.removeAttribute("list");
        } else {
            this.list = this.input.getAttribute("data-list") || o.list || [];
        }

        _.all.push(this);
    };

    _.prototype = {
        set list(list) {
            if (Array.isArray(list)) {
                this._list = list;
            } else if (typeof list === "string" && list.indexOf(",") > -1) {
                this._list = list.split(/\s*,\s*/);
            } else {
                // Element or CSS selector
                list = $(list);

                if (list && list.children) {
                    var items = [];
                    slice.apply(list.children).forEach(function (el) {
                        if (!el.disabled) {
                            var text = el.textContent.trim();
                            var value = el.value || text;
                            var label = el.label || text;
                            if (value !== "") {
                                items.push({ label: label, value: value });
                            }
                        }
                    });
                    this._list = items;
                }
            }

            if (document.activeElement === this.input) {
                this.evaluate();
            }
        },

        get selected() {
            return this.index > -1;
        },

        get opened() {
            return this.isOpened;
        },

        close: function close(o) {
            if (!this.opened) {
                return;
            }

            this.ul.setAttribute("hidden", "");
            this.isOpened = false;
            this.index = -1;

            $.fire(this.input, "awesomplete-close", o || {});
        },

        open: function open() {
            this.ul.removeAttribute("hidden");
            this.isOpened = true;

            if (this.autoFirst && this.index === -1) {
                this.goto(0);
            }

            $.fire(this.input, "awesomplete-open");
        },

        destroy: function destroy() {
            //remove events from the input and its form
            $.unbind(this.input, this._events.input);
            $.unbind(this.input.form, this._events.form);

            //move the input out of the awesomplete container and remove the container and its children
            var parentNode = this.container.parentNode;

            parentNode.insertBefore(this.input, this.container);
            parentNode.removeChild(this.container);

            //remove autocomplete and aria-autocomplete attributes
            this.input.removeAttribute("autocomplete");
            this.input.removeAttribute("aria-autocomplete");

            //remove this awesomeplete instance from the global array of instances
            var indexOfAwesomplete = _.all.indexOf(this);

            if (indexOfAwesomplete !== -1) {
                _.all.splice(indexOfAwesomplete, 1);
            }
        },

        next: function next() {
            var count = this.ul.children.length;
            this.goto(this.index < count - 1 ? this.index + 1 : count ? 0 : -1);
        },

        previous: function previous() {
            var count = this.ul.children.length;
            var pos = this.index - 1;

            this.goto(this.selected && pos !== -1 ? pos : count - 1);
        },

        // Should not be used, highlights specific item without any checks!
        goto: function goto(i) {
            var lis = this.ul.children;

            if (this.selected) {
                lis[this.index].setAttribute("aria-selected", "false");
            }

            this.index = i;

            if (i > -1 && lis.length > 0) {
                lis[i].setAttribute("aria-selected", "true");

                this.status.textContent = lis[i].textContent + ", list item " + (i + 1) + " of " + lis.length;

                this.input.setAttribute("aria-activedescendant", this.ul.id + "_item_" + this.index);

                // scroll to highlighted element in case parent's height is fixed
                this.ul.scrollTop = lis[i].offsetTop - this.ul.clientHeight + lis[i].clientHeight;

                $.fire(this.input, "awesomplete-highlight", {
                    text: this.suggestions[this.index]
                });
            }
        },

        select: function select(selected, origin) {
            if (selected) {
                this.index = $.siblingIndex(selected);
            } else {
                selected = this.ul.children[this.index];
            }

            if (selected) {
                var suggestion = this.suggestions[this.index];

                var allowed = $.fire(this.input, "awesomplete-select", {
                    text: suggestion,
                    origin: origin || selected
                });

                if (allowed) {
                    this.replace(suggestion);
                    this.close({ reason: "select" });
                    $.fire(this.input, "awesomplete-selectcomplete", {
                        text: suggestion
                    });
                }
            }
        },

        evaluate: function evaluate() {
            var me = this;
            var value = this.input.value;

            if (value.length >= this.minChars && this._list.length > 0) {
                this.index = -1;
                // Populate list with options that match
                this.ul.innerHTML = "";

                this.suggestions = this._list.map(function (item) {
                    return new Suggestion(me.data(item, value));
                }).filter(function (item) {
                    return me.filter(item, value);
                });

                if (this.sort !== false) {
                    this.suggestions = this.suggestions.sort(this.sort);
                }

                this.suggestions = this.suggestions.slice(0, this.maxItems);

                this.suggestions.forEach(function (text, index) {
                    me.ul.appendChild(me.item(text, value, index));
                });

                if (this.ul.children.length === 0) {

                    this.status.textContent = "No results found";

                    this.close({ reason: "nomatches" });
                } else {
                    this.open();

                    this.status.textContent = this.ul.children.length + " results found";
                }
            } else {
                this.close({ reason: "nomatches" });

                this.status.textContent = "No results found";
            }
        }
    };

    // Static methods/properties

    _.all = [];

    _.FILTER_CONTAINS = function (text, input) {
        return RegExp($.regExpEscape(input.trim()), "i").test(text);
    };

    _.FILTER_STARTSWITH = function (text, input) {
        return RegExp("^" + $.regExpEscape(input.trim()), "i").test(text);
    };

    _.SORT_BYLENGTH = function (a, b) {
        if (a.length !== b.length) {
            return a.length - b.length;
        }

        return a < b ? -1 : 1;
    };

    _.ITEM = function (text, input, item_id) {
        var html = input.trim() === "" ? text : text.replace(RegExp($.regExpEscape(input.trim()), "gi"), "<mark>$&</mark>");
        return $.create("li", {
            innerHTML: html,
            "aria-selected": "false",
            "id": "awesomplete_list_" + this.count + "_item_" + item_id
        });
    };

    _.REPLACE = function (text) {
        this.input.value = text.value;
    };

    _.DATA = function (item /*, input*/) {
        return item;
    };

    // Private functions

    function Suggestion(data) {
        var o = Array.isArray(data) ? { label: data[0], value: data[1] } : (typeof data === "undefined" ? "undefined" : _typeof(data)) === "object" && "label" in data && "value" in data ? data : { label: data, value: data };

        this.label = o.label || o.value;
        this.value = o.value;
    }
    Object.defineProperty(Suggestion.prototype = Object.create(String.prototype), "length", {
        get: function get() {
            return this.label.length;
        }
    });
    Suggestion.prototype.toString = Suggestion.prototype.valueOf = function () {
        return "" + this.label;
    };

    function configure(instance, properties, o) {
        for (var i in properties) {
            var initial = properties[i],
                attrValue = instance.input.getAttribute("data-" + i.toLowerCase());

            if (typeof initial === "number") {
                instance[i] = parseInt(attrValue);
            } else if (initial === false) {
                // Boolean options must be false by default anyway
                instance[i] = attrValue !== null;
            } else if (initial instanceof Function) {
                instance[i] = null;
            } else {
                instance[i] = attrValue;
            }

            if (!instance[i] && instance[i] !== 0) {
                instance[i] = i in o ? o[i] : initial;
            }
        }
    }

    // Helpers

    var slice = Array.prototype.slice;

    function $(expr, con) {
        return typeof expr === "string" ? (con || document).querySelector(expr) : expr || null;
    }

    function $$(expr, con) {
        return slice.call((con || document).querySelectorAll(expr));
    }

    $.create = function (tag, o) {
        var element = document.createElement(tag);

        for (var i in o) {
            var val = o[i];

            if (i === "inside") {
                $(val).appendChild(element);
            } else if (i === "around") {
                var ref = $(val);
                ref.parentNode.insertBefore(element, ref);
                element.appendChild(ref);
            } else if (i in element) {
                element[i] = val;
            } else {
                element.setAttribute(i, val);
            }
        }

        return element;
    };

    $.bind = function (element, o) {
        if (element) {
            for (var event in o) {
                var callback = o[event];

                event.split(/\s+/).forEach(function (event) {
                    element.addEventListener(event, callback);
                });
            }
        }
    };

    $.unbind = function (element, o) {
        if (element) {
            for (var event in o) {
                var callback = o[event];

                event.split(/\s+/).forEach(function (event) {
                    element.removeEventListener(event, callback);
                });
            }
        }
    };

    $.fire = function (target, type, properties) {
        var evt = document.createEvent("HTMLEvents");

        evt.initEvent(type, true, true);

        for (var j in properties) {
            evt[j] = properties[j];
        }

        return target.dispatchEvent(evt);
    };

    $.regExpEscape = function (s) {
        return s.replace(/[-\\^$*+?.()|[\]{}]/g, "\\$&");
    };

    $.siblingIndex = function (el) {
        /* eslint-disable no-cond-assign */
        for (var i = 0; el = el.previousElementSibling; i++) {}
        return i;
    };

    // Initialization

    function init() {
        $$("input.awesomplete").forEach(function (input) {
            new _(input);
        });
    }

    // Are we in a browser? Check for Document constructor
    if (typeof Document !== "undefined") {
        // DOM already loaded?
        if (document.readyState !== "loading") {
            init();
        } else {
            // Wait for it
            document.addEventListener("DOMContentLoaded", init);
        }
    }

    _.$ = $;
    _.$$ = $$;

    // Make sure to export Awesomplete on self when in a browser
    if (typeof self !== "undefined") {
        self.Awesomplete = _;
    }

    // Expose Awesomplete as a CJS module
    if ((typeof module === "undefined" ? "undefined" : _typeof(module)) === "object" && module.exports) {
        module.exports = _;
    }

    return _;
})();
/* jshint ignore:end */

// end file: _autocomplete.js

// file: _progressbar.js
/* jshint ignore:start */
!function (a) {
    if ("object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module) module.exports = a();else if ("function" == typeof define && define.amd) define([], a);else {
        var b;b = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, b.ProgressBar = a();
    }
}(function () {
    var a;return function a(b, c, d) {
        function e(g, h) {
            if (!c[g]) {
                if (!b[g]) {
                    var i = "function" == typeof require && require;if (!h && i) return i(g, !0);if (f) return f(g, !0);var j = new Error("Cannot find module '" + g + "'");throw j.code = "MODULE_NOT_FOUND", j;
                }var k = c[g] = { exports: {} };b[g][0].call(k.exports, function (a) {
                    var c = b[g][1][a];return e(c ? c : a);
                }, k, k.exports, a, b, c, d);
            }return c[g].exports;
        }for (var f = "function" == typeof require && require, g = 0; g < d.length; g++) {
            e(d[g]);
        }return e;
    }({ 1: [function (b, c, d) {
            (function () {
                var b = this || Function("return this")(),
                    e = function () {
                    "use strict";
                    function e() {}function f(a, b) {
                        var c;for (c in a) {
                            Object.hasOwnProperty.call(a, c) && b(c);
                        }
                    }function g(a, b) {
                        return f(b, function (c) {
                            a[c] = b[c];
                        }), a;
                    }function h(a, b) {
                        f(b, function (c) {
                            "undefined" == typeof a[c] && (a[c] = b[c]);
                        });
                    }function i(a, b, c, d, e, f, g) {
                        var h,
                            i,
                            k,
                            l = a < f ? 0 : (a - f) / e;for (h in b) {
                            b.hasOwnProperty(h) && (i = g[h], k = "function" == typeof i ? i : o[i], b[h] = j(c[h], d[h], k, l));
                        }return b;
                    }function j(a, b, c, d) {
                        return a + (b - a) * c(d);
                    }function k(a, b) {
                        var c = n.prototype.filter,
                            d = a._filterArgs;f(c, function (e) {
                            "undefined" != typeof c[e][b] && c[e][b].apply(a, d);
                        });
                    }function l(a, b, c, d, e, f, g, h, j, l, m) {
                        v = b + c + d, w = Math.min(m || u(), v), x = w >= v, y = d - (v - w), a.isPlaying() && (x ? (j(g, a._attachment, y), a.stop(!0)) : (a._scheduleId = l(a._timeoutHandler, s), k(a, "beforeTween"), w < b + c ? i(1, e, f, g, 1, 1, h) : i(w, e, f, g, d, b + c, h), k(a, "afterTween"), j(e, a._attachment, y)));
                    }function m(a, b) {
                        var c = {},
                            d = typeof b === "undefined" ? "undefined" : _typeof(b);return "string" === d || "function" === d ? f(a, function (a) {
                            c[a] = b;
                        }) : f(a, function (a) {
                            c[a] || (c[a] = b[a] || q);
                        }), c;
                    }function n(a, b) {
                        this._currentState = a || {}, this._configured = !1, this._scheduleFunction = p, "undefined" != typeof b && this.setConfig(b);
                    }var o,
                        p,
                        q = "linear",
                        r = 500,
                        s = 1e3 / 60,
                        t = Date.now ? Date.now : function () {
                        return +new Date();
                    },
                        u = "undefined" != typeof SHIFTY_DEBUG_NOW ? SHIFTY_DEBUG_NOW : t;p = "undefined" != typeof window ? window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || window.mozCancelRequestAnimationFrame && window.mozRequestAnimationFrame || setTimeout : setTimeout;var v, w, x, y;return n.prototype.tween = function (a) {
                        return this._isTweening ? this : (void 0 === a && this._configured || this.setConfig(a), this._timestamp = u(), this._start(this.get(), this._attachment), this.resume());
                    }, n.prototype.setConfig = function (a) {
                        a = a || {}, this._configured = !0, this._attachment = a.attachment, this._pausedAtTime = null, this._scheduleId = null, this._delay = a.delay || 0, this._start = a.start || e, this._step = a.step || e, this._finish = a.finish || e, this._duration = a.duration || r, this._currentState = g({}, a.from || this.get()), this._originalState = this.get(), this._targetState = g({}, a.to || this.get());var b = this;this._timeoutHandler = function () {
                            l(b, b._timestamp, b._delay, b._duration, b._currentState, b._originalState, b._targetState, b._easing, b._step, b._scheduleFunction);
                        };var c = this._currentState,
                            d = this._targetState;return h(d, c), this._easing = m(c, a.easing || q), this._filterArgs = [c, this._originalState, d, this._easing], k(this, "tweenCreated"), this;
                    }, n.prototype.get = function () {
                        return g({}, this._currentState);
                    }, n.prototype.set = function (a) {
                        this._currentState = a;
                    }, n.prototype.pause = function () {
                        return this._pausedAtTime = u(), this._isPaused = !0, this;
                    }, n.prototype.resume = function () {
                        return this._isPaused && (this._timestamp += u() - this._pausedAtTime), this._isPaused = !1, this._isTweening = !0, this._timeoutHandler(), this;
                    }, n.prototype.seek = function (a) {
                        a = Math.max(a, 0);var b = u();return this._timestamp + a === 0 ? this : (this._timestamp = b - a, this.isPlaying() || (this._isTweening = !0, this._isPaused = !1, l(this, this._timestamp, this._delay, this._duration, this._currentState, this._originalState, this._targetState, this._easing, this._step, this._scheduleFunction, b), this.pause()), this);
                    }, n.prototype.stop = function (a) {
                        return this._isTweening = !1, this._isPaused = !1, this._timeoutHandler = e, (b.cancelAnimationFrame || b.webkitCancelAnimationFrame || b.oCancelAnimationFrame || b.msCancelAnimationFrame || b.mozCancelRequestAnimationFrame || b.clearTimeout)(this._scheduleId), a && (k(this, "beforeTween"), i(1, this._currentState, this._originalState, this._targetState, 1, 0, this._easing), k(this, "afterTween"), k(this, "afterTweenEnd"), this._finish.call(this, this._currentState, this._attachment)), this;
                    }, n.prototype.isPlaying = function () {
                        return this._isTweening && !this._isPaused;
                    }, n.prototype.setScheduleFunction = function (a) {
                        this._scheduleFunction = a;
                    }, n.prototype.dispose = function () {
                        var a;for (a in this) {
                            this.hasOwnProperty(a) && delete this[a];
                        }
                    }, n.prototype.filter = {}, n.prototype.formula = { linear: function linear(a) {
                            return a;
                        } }, o = n.prototype.formula, g(n, { now: u, each: f, tweenProps: i, tweenProp: j, applyFilter: k, shallowCopy: g, defaults: h, composeEasingObject: m }), "function" == typeof SHIFTY_DEBUG_NOW && (b.timeoutHandler = l), "object" == (typeof d === "undefined" ? "undefined" : _typeof(d)) ? c.exports = n : "function" == typeof a && a.amd ? a(function () {
                        return n;
                    }) : "undefined" == typeof b.Tweenable && (b.Tweenable = n), n;
                }();!function () {
                    e.shallowCopy(e.prototype.formula, { easeInQuad: function easeInQuad(a) {
                            return Math.pow(a, 2);
                        }, easeOutQuad: function easeOutQuad(a) {
                            return -(Math.pow(a - 1, 2) - 1);
                        }, easeInOutQuad: function easeInOutQuad(a) {
                            return (a /= .5) < 1 ? .5 * Math.pow(a, 2) : -.5 * ((a -= 2) * a - 2);
                        }, easeInCubic: function easeInCubic(a) {
                            return Math.pow(a, 3);
                        }, easeOutCubic: function easeOutCubic(a) {
                            return Math.pow(a - 1, 3) + 1;
                        }, easeInOutCubic: function easeInOutCubic(a) {
                            return (a /= .5) < 1 ? .5 * Math.pow(a, 3) : .5 * (Math.pow(a - 2, 3) + 2);
                        }, easeInQuart: function easeInQuart(a) {
                            return Math.pow(a, 4);
                        }, easeOutQuart: function easeOutQuart(a) {
                            return -(Math.pow(a - 1, 4) - 1);
                        }, easeInOutQuart: function easeInOutQuart(a) {
                            return (a /= .5) < 1 ? .5 * Math.pow(a, 4) : -.5 * ((a -= 2) * Math.pow(a, 3) - 2);
                        }, easeInQuint: function easeInQuint(a) {
                            return Math.pow(a, 5);
                        }, easeOutQuint: function easeOutQuint(a) {
                            return Math.pow(a - 1, 5) + 1;
                        }, easeInOutQuint: function easeInOutQuint(a) {
                            return (a /= .5) < 1 ? .5 * Math.pow(a, 5) : .5 * (Math.pow(a - 2, 5) + 2);
                        }, easeInSine: function easeInSine(a) {
                            return -Math.cos(a * (Math.PI / 2)) + 1;
                        }, easeOutSine: function easeOutSine(a) {
                            return Math.sin(a * (Math.PI / 2));
                        }, easeInOutSine: function easeInOutSine(a) {
                            return -.5 * (Math.cos(Math.PI * a) - 1);
                        }, easeInExpo: function easeInExpo(a) {
                            return 0 === a ? 0 : Math.pow(2, 10 * (a - 1));
                        }, easeOutExpo: function easeOutExpo(a) {
                            return 1 === a ? 1 : -Math.pow(2, -10 * a) + 1;
                        }, easeInOutExpo: function easeInOutExpo(a) {
                            return 0 === a ? 0 : 1 === a ? 1 : (a /= .5) < 1 ? .5 * Math.pow(2, 10 * (a - 1)) : .5 * (-Math.pow(2, -10 * --a) + 2);
                        }, easeInCirc: function easeInCirc(a) {
                            return -(Math.sqrt(1 - a * a) - 1);
                        }, easeOutCirc: function easeOutCirc(a) {
                            return Math.sqrt(1 - Math.pow(a - 1, 2));
                        }, easeInOutCirc: function easeInOutCirc(a) {
                            return (a /= .5) < 1 ? -.5 * (Math.sqrt(1 - a * a) - 1) : .5 * (Math.sqrt(1 - (a -= 2) * a) + 1);
                        }, easeOutBounce: function easeOutBounce(a) {
                            return a < 1 / 2.75 ? 7.5625 * a * a : a < 2 / 2.75 ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : a < 2.5 / 2.75 ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375;
                        }, easeInBack: function easeInBack(a) {
                            var b = 1.70158;return a * a * ((b + 1) * a - b);
                        }, easeOutBack: function easeOutBack(a) {
                            var b = 1.70158;return (a -= 1) * a * ((b + 1) * a + b) + 1;
                        }, easeInOutBack: function easeInOutBack(a) {
                            var b = 1.70158;return (a /= .5) < 1 ? .5 * (a * a * (((b *= 1.525) + 1) * a - b)) : .5 * ((a -= 2) * a * (((b *= 1.525) + 1) * a + b) + 2);
                        }, elastic: function elastic(a) {
                            return -1 * Math.pow(4, -8 * a) * Math.sin((6 * a - 1) * (2 * Math.PI) / 2) + 1;
                        }, swingFromTo: function swingFromTo(a) {
                            var b = 1.70158;return (a /= .5) < 1 ? .5 * (a * a * (((b *= 1.525) + 1) * a - b)) : .5 * ((a -= 2) * a * (((b *= 1.525) + 1) * a + b) + 2);
                        }, swingFrom: function swingFrom(a) {
                            var b = 1.70158;return a * a * ((b + 1) * a - b);
                        }, swingTo: function swingTo(a) {
                            var b = 1.70158;return (a -= 1) * a * ((b + 1) * a + b) + 1;
                        }, bounce: function bounce(a) {
                            return a < 1 / 2.75 ? 7.5625 * a * a : a < 2 / 2.75 ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : a < 2.5 / 2.75 ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375;
                        }, bouncePast: function bouncePast(a) {
                            return a < 1 / 2.75 ? 7.5625 * a * a : a < 2 / 2.75 ? 2 - (7.5625 * (a -= 1.5 / 2.75) * a + .75) : a < 2.5 / 2.75 ? 2 - (7.5625 * (a -= 2.25 / 2.75) * a + .9375) : 2 - (7.5625 * (a -= 2.625 / 2.75) * a + .984375);
                        }, easeFromTo: function easeFromTo(a) {
                            return (a /= .5) < 1 ? .5 * Math.pow(a, 4) : -.5 * ((a -= 2) * Math.pow(a, 3) - 2);
                        }, easeFrom: function easeFrom(a) {
                            return Math.pow(a, 4);
                        }, easeTo: function easeTo(a) {
                            return Math.pow(a, .25);
                        } });
                }(), function () {
                    function a(a, b, c, d, e, f) {
                        function g(a) {
                            return ((n * a + o) * a + p) * a;
                        }function h(a) {
                            return ((q * a + r) * a + s) * a;
                        }function i(a) {
                            return (3 * n * a + 2 * o) * a + p;
                        }function j(a) {
                            return 1 / (200 * a);
                        }function k(a, b) {
                            return h(m(a, b));
                        }function l(a) {
                            return a >= 0 ? a : 0 - a;
                        }function m(a, b) {
                            var c, d, e, f, h, j;for (e = a, j = 0; j < 8; j++) {
                                if (f = g(e) - a, l(f) < b) return e;if (h = i(e), l(h) < 1e-6) break;e -= f / h;
                            }if (c = 0, d = 1, e = a, e < c) return c;if (e > d) return d;for (; c < d;) {
                                if (f = g(e), l(f - a) < b) return e;a > f ? c = e : d = e, e = .5 * (d - c) + c;
                            }return e;
                        }var n = 0,
                            o = 0,
                            p = 0,
                            q = 0,
                            r = 0,
                            s = 0;return p = 3 * b, o = 3 * (d - b) - p, n = 1 - p - o, s = 3 * c, r = 3 * (e - c) - s, q = 1 - s - r, k(a, j(f));
                    }function b(b, c, d, e) {
                        return function (f) {
                            return a(f, b, c, d, e, 1);
                        };
                    }e.setBezierFunction = function (a, c, d, f, g) {
                        var h = b(c, d, f, g);return h.displayName = a, h.x1 = c, h.y1 = d, h.x2 = f, h.y2 = g, e.prototype.formula[a] = h;
                    }, e.unsetBezierFunction = function (a) {
                        delete e.prototype.formula[a];
                    };
                }(), function () {
                    function a(a, b, c, d, f, g) {
                        return e.tweenProps(d, b, a, c, 1, g, f);
                    }var b = new e();b._filterArgs = [], e.interpolate = function (c, d, f, g, h) {
                        var i = e.shallowCopy({}, c),
                            j = h || 0,
                            k = e.composeEasingObject(c, g || "linear");b.set({});var l = b._filterArgs;l.length = 0, l[0] = i, l[1] = c, l[2] = d, l[3] = k, e.applyFilter(b, "tweenCreated"), e.applyFilter(b, "beforeTween");var m = a(c, i, d, f, k, j);return e.applyFilter(b, "afterTween"), m;
                    };
                }(), function (a) {
                    function b(a, b) {
                        var c,
                            d = [],
                            e = a.length;for (c = 0; c < e; c++) {
                            d.push("_" + b + "_" + c);
                        }return d;
                    }function c(a) {
                        var b = a.match(v);return b ? (1 === b.length || a.charAt(0).match(u)) && b.unshift("") : b = ["", ""], b.join(A);
                    }function d(b) {
                        a.each(b, function (a) {
                            var c = b[a];"string" == typeof c && c.match(z) && (b[a] = e(c));
                        });
                    }function e(a) {
                        return i(z, a, f);
                    }function f(a) {
                        var b = g(a);return "rgb(" + b[0] + "," + b[1] + "," + b[2] + ")";
                    }function g(a) {
                        return a = a.replace(/#/, ""), 3 === a.length && (a = a.split(""), a = a[0] + a[0] + a[1] + a[1] + a[2] + a[2]), B[0] = h(a.substr(0, 2)), B[1] = h(a.substr(2, 2)), B[2] = h(a.substr(4, 2)), B;
                    }function h(a) {
                        return parseInt(a, 16);
                    }function i(a, b, c) {
                        var d = b.match(a),
                            e = b.replace(a, A);if (d) for (var f, g = d.length, h = 0; h < g; h++) {
                            f = d.shift(), e = e.replace(A, c(f));
                        }return e;
                    }function j(a) {
                        return i(x, a, k);
                    }function k(a) {
                        for (var b = a.match(w), c = b.length, d = a.match(y)[0], e = 0; e < c; e++) {
                            d += parseInt(b[e], 10) + ",";
                        }return d = d.slice(0, -1) + ")";
                    }function l(d) {
                        var e = {};return a.each(d, function (a) {
                            var f = d[a];if ("string" == typeof f) {
                                var g = r(f);e[a] = { formatString: c(f), chunkNames: b(g, a) };
                            }
                        }), e;
                    }function m(b, c) {
                        a.each(c, function (a) {
                            for (var d = b[a], e = r(d), f = e.length, g = 0; g < f; g++) {
                                b[c[a].chunkNames[g]] = +e[g];
                            }delete b[a];
                        });
                    }function n(b, c) {
                        a.each(c, function (a) {
                            var d = b[a],
                                e = o(b, c[a].chunkNames),
                                f = p(e, c[a].chunkNames);d = q(c[a].formatString, f), b[a] = j(d);
                        });
                    }function o(a, b) {
                        for (var c, d = {}, e = b.length, f = 0; f < e; f++) {
                            c = b[f], d[c] = a[c], delete a[c];
                        }return d;
                    }function p(a, b) {
                        C.length = 0;for (var c = b.length, d = 0; d < c; d++) {
                            C.push(a[b[d]]);
                        }return C;
                    }function q(a, b) {
                        for (var c = a, d = b.length, e = 0; e < d; e++) {
                            c = c.replace(A, +b[e].toFixed(4));
                        }return c;
                    }function r(a) {
                        return a.match(w);
                    }function s(b, c) {
                        a.each(c, function (a) {
                            var d,
                                e = c[a],
                                f = e.chunkNames,
                                g = f.length,
                                h = b[a];if ("string" == typeof h) {
                                var i = h.split(" "),
                                    j = i[i.length - 1];for (d = 0; d < g; d++) {
                                    b[f[d]] = i[d] || j;
                                }
                            } else for (d = 0; d < g; d++) {
                                b[f[d]] = h;
                            }delete b[a];
                        });
                    }function t(b, c) {
                        a.each(c, function (a) {
                            var d = c[a],
                                e = d.chunkNames,
                                f = e.length,
                                g = b[e[0]],
                                h = typeof g === "undefined" ? "undefined" : _typeof(g);if ("string" === h) {
                                for (var i = "", j = 0; j < f; j++) {
                                    i += " " + b[e[j]], delete b[e[j]];
                                }b[a] = i.substr(1);
                            } else b[a] = g;
                        });
                    }var u = /(\d|\-|\.)/,
                        v = /([^\-0-9\.]+)/g,
                        w = /[0-9.\-]+/g,
                        x = new RegExp("rgb\\(" + w.source + /,\s*/.source + w.source + /,\s*/.source + w.source + "\\)", "g"),
                        y = /^.*\(/,
                        z = /#([0-9]|[a-f]){3,6}/gi,
                        A = "VAL",
                        B = [],
                        C = [];a.prototype.filter.token = { tweenCreated: function tweenCreated(a, b, c, e) {
                            d(a), d(b), d(c), this._tokenData = l(a);
                        }, beforeTween: function beforeTween(a, b, c, d) {
                            s(d, this._tokenData), m(a, this._tokenData), m(b, this._tokenData), m(c, this._tokenData);
                        }, afterTween: function afterTween(a, b, c, d) {
                            n(a, this._tokenData), n(b, this._tokenData), n(c, this._tokenData), t(d, this._tokenData);
                        } };
                }(e);
            }).call(null);
        }, {}], 2: [function (a, b, c) {
            var d = a("./shape"),
                e = a("./utils"),
                f = function f(a, b) {
                this._pathTemplate = "M 50,50 m 0,-{radius} a {radius},{radius} 0 1 1 0,{2radius} a {radius},{radius} 0 1 1 0,-{2radius}", this.containerAspectRatio = 1, d.apply(this, arguments);
            };f.prototype = new d(), f.prototype.constructor = f, f.prototype._pathString = function (a) {
                var b = a.strokeWidth;a.trailWidth && a.trailWidth > a.strokeWidth && (b = a.trailWidth);var c = 50 - b / 2;return e.render(this._pathTemplate, { radius: c, "2radius": 2 * c });
            }, f.prototype._trailString = function (a) {
                return this._pathString(a);
            }, b.exports = f;
        }, { "./shape": 7, "./utils": 9 }], 3: [function (a, b, c) {
            var d = a("./shape"),
                e = a("./utils"),
                f = function f(a, b) {
                this._pathTemplate = "M 0,{center} L 100,{center}", d.apply(this, arguments);
            };f.prototype = new d(), f.prototype.constructor = f, f.prototype._initializeSvg = function (a, b) {
                a.setAttribute("viewBox", "0 0 100 " + b.strokeWidth), a.setAttribute("preserveAspectRatio", "none");
            }, f.prototype._pathString = function (a) {
                return e.render(this._pathTemplate, { center: a.strokeWidth / 2 });
            }, f.prototype._trailString = function (a) {
                return this._pathString(a);
            }, b.exports = f;
        }, { "./shape": 7, "./utils": 9 }], 4: [function (a, b, c) {
            b.exports = { Line: a("./line"), Circle: a("./circle"), SemiCircle: a("./semicircle"), Square: a("./square"), Path: a("./path"), Shape: a("./shape"), utils: a("./utils") };
        }, { "./circle": 2, "./line": 3, "./path": 5, "./semicircle": 6, "./shape": 7, "./square": 8, "./utils": 9 }], 5: [function (a, b, c) {
            var d = a("shifty"),
                e = a("./utils"),
                f = { easeIn: "easeInCubic", easeOut: "easeOutCubic", easeInOut: "easeInOutCubic" },
                g = function a(b, c) {
                if (!(this instanceof a)) throw new Error("Constructor was called without new keyword");c = e.extend({ duration: 800, easing: "linear", from: {}, to: {}, step: function step() {} }, c);var d;d = e.isString(b) ? document.querySelector(b) : b, this.path = d, this._opts = c, this._tweenable = null;var f = this.path.getTotalLength();this.path.style.strokeDasharray = f + " " + f, this.set(0);
            };g.prototype.value = function () {
                var a = this._getComputedDashOffset(),
                    b = this.path.getTotalLength(),
                    c = 1 - a / b;return parseFloat(c.toFixed(6), 10);
            }, g.prototype.set = function (a) {
                this.stop(), this.path.style.strokeDashoffset = this._progressToOffset(a);var b = this._opts.step;if (e.isFunction(b)) {
                    var c = this._easing(this._opts.easing),
                        d = this._calculateTo(a, c),
                        f = this._opts.shape || this;b(d, f, this._opts.attachment);
                }
            }, g.prototype.stop = function () {
                this._stopTween(), this.path.style.strokeDashoffset = this._getComputedDashOffset();
            }, g.prototype.animate = function (a, b, c) {
                b = b || {}, e.isFunction(b) && (c = b, b = {});var f = e.extend({}, b),
                    g = e.extend({}, this._opts);b = e.extend(g, b);var h = this._easing(b.easing),
                    i = this._resolveFromAndTo(a, h, f);this.stop(), this.path.getBoundingClientRect();var j = this._getComputedDashOffset(),
                    k = this._progressToOffset(a),
                    l = this;this._tweenable = new d(), this._tweenable.tween({ from: e.extend({ offset: j }, i.from), to: e.extend({ offset: k }, i.to), duration: b.duration, easing: h, step: function step(a) {
                        l.path.style.strokeDashoffset = a.offset;var c = b.shape || l;b.step(a, c, b.attachment);
                    }, finish: function finish(a) {
                        e.isFunction(c) && c();
                    } });
            }, g.prototype._getComputedDashOffset = function () {
                var a = window.getComputedStyle(this.path, null);return parseFloat(a.getPropertyValue("stroke-dashoffset"), 10);
            }, g.prototype._progressToOffset = function (a) {
                var b = this.path.getTotalLength();return b - a * b;
            }, g.prototype._resolveFromAndTo = function (a, b, c) {
                return c.from && c.to ? { from: c.from, to: c.to } : { from: this._calculateFrom(b), to: this._calculateTo(a, b) };
            }, g.prototype._calculateFrom = function (a) {
                return d.interpolate(this._opts.from, this._opts.to, this.value(), a);
            }, g.prototype._calculateTo = function (a, b) {
                return d.interpolate(this._opts.from, this._opts.to, a, b);
            }, g.prototype._stopTween = function () {
                null !== this._tweenable && (this._tweenable.stop(), this._tweenable = null);
            }, g.prototype._easing = function (a) {
                return f.hasOwnProperty(a) ? f[a] : a;
            }, b.exports = g;
        }, { "./utils": 9, shifty: 1 }], 6: [function (a, b, c) {
            var d = a("./shape"),
                e = a("./circle"),
                f = a("./utils"),
                g = function g(a, b) {
                this._pathTemplate = "M 50,50 m -{radius},0 a {radius},{radius} 0 1 1 {2radius},0", this.containerAspectRatio = 2, d.apply(this, arguments);
            };g.prototype = new d(), g.prototype.constructor = g, g.prototype._initializeSvg = function (a, b) {
                a.setAttribute("viewBox", "0 0 100 50");
            }, g.prototype._initializeTextContainer = function (a, b, c) {
                a.text.style && (c.style.top = "auto", c.style.bottom = "0", a.text.alignToBottom ? f.setStyle(c, "transform", "translate(-50%, 0)") : f.setStyle(c, "transform", "translate(-50%, 50%)"));
            }, g.prototype._pathString = e.prototype._pathString, g.prototype._trailString = e.prototype._trailString, b.exports = g;
        }, { "./circle": 2, "./shape": 7, "./utils": 9 }], 7: [function (a, b, c) {
            var d = a("./path"),
                e = a("./utils"),
                f = "Object is destroyed",
                g = function a(b, c) {
                if (!(this instanceof a)) throw new Error("Constructor was called without new keyword");if (0 !== arguments.length) {
                    this._opts = e.extend({ color: "#555", strokeWidth: 1, trailColor: null, trailWidth: null, fill: null, text: { style: { color: null, position: "absolute", left: "50%", top: "50%", padding: 0, margin: 0, transform: { prefix: !0, value: "translate(-50%, -50%)" } }, autoStyleContainer: !0, alignToBottom: !0, value: null, className: "progressbar-text" }, svgStyle: { display: "block", width: "100%" }, warnings: !1 }, c, !0), e.isObject(c) && void 0 !== c.svgStyle && (this._opts.svgStyle = c.svgStyle), e.isObject(c) && e.isObject(c.text) && void 0 !== c.text.style && (this._opts.text.style = c.text.style);var f,
                        g = this._createSvgView(this._opts);if (f = e.isString(b) ? document.querySelector(b) : b, !f) throw new Error("Container does not exist: " + b);this._container = f, this._container.appendChild(g.svg), this._opts.warnings && this._warnContainerAspectRatio(this._container), this._opts.svgStyle && e.setStyles(g.svg, this._opts.svgStyle), this.svg = g.svg, this.path = g.path, this.trail = g.trail, this.text = null;var h = e.extend({ attachment: void 0, shape: this }, this._opts);this._progressPath = new d(g.path, h), e.isObject(this._opts.text) && null !== this._opts.text.value && this.setText(this._opts.text.value);
                }
            };g.prototype.animate = function (a, b, c) {
                if (null === this._progressPath) throw new Error(f);this._progressPath.animate(a, b, c);
            }, g.prototype.stop = function () {
                if (null === this._progressPath) throw new Error(f);void 0 !== this._progressPath && this._progressPath.stop();
            }, g.prototype.destroy = function () {
                if (null === this._progressPath) throw new Error(f);this.stop(), this.svg.parentNode.removeChild(this.svg), this.svg = null, this.path = null, this.trail = null, this._progressPath = null, null !== this.text && (this.text.parentNode.removeChild(this.text), this.text = null);
            }, g.prototype.set = function (a) {
                if (null === this._progressPath) throw new Error(f);this._progressPath.set(a);
            }, g.prototype.value = function () {
                if (null === this._progressPath) throw new Error(f);return void 0 === this._progressPath ? 0 : this._progressPath.value();
            }, g.prototype.setText = function (a) {
                if (null === this._progressPath) throw new Error(f);null === this.text && (this.text = this._createTextContainer(this._opts, this._container), this._container.appendChild(this.text)), e.isObject(a) ? (e.removeChildren(this.text), this.text.appendChild(a)) : this.text.innerHTML = a;
            }, g.prototype._createSvgView = function (a) {
                var b = document.createElementNS("http://www.w3.org/2000/svg", "svg");this._initializeSvg(b, a);var c = null;(a.trailColor || a.trailWidth) && (c = this._createTrail(a), b.appendChild(c));var d = this._createPath(a);return b.appendChild(d), { svg: b, path: d, trail: c };
            }, g.prototype._initializeSvg = function (a, b) {
                a.setAttribute("viewBox", "0 0 100 100");
            }, g.prototype._createPath = function (a) {
                var b = this._pathString(a);return this._createPathElement(b, a);
            }, g.prototype._createTrail = function (a) {
                var b = this._trailString(a),
                    c = e.extend({}, a);return c.trailColor || (c.trailColor = "#eee"), c.trailWidth || (c.trailWidth = c.strokeWidth), c.color = c.trailColor, c.strokeWidth = c.trailWidth, c.fill = null, this._createPathElement(b, c);
            }, g.prototype._createPathElement = function (a, b) {
                var c = document.createElementNS("http://www.w3.org/2000/svg", "path");return c.setAttribute("d", a), c.setAttribute("stroke", b.color), c.setAttribute("stroke-width", b.strokeWidth), b.fill ? c.setAttribute("fill", b.fill) : c.setAttribute("fill-opacity", "0"), c;
            }, g.prototype._createTextContainer = function (a, b) {
                var c = document.createElement("div");c.className = a.text.className;var d = a.text.style;return d && (a.text.autoStyleContainer && (b.style.position = "relative"), e.setStyles(c, d), d.color || (c.style.color = a.color)), this._initializeTextContainer(a, b, c), c;
            }, g.prototype._initializeTextContainer = function (a, b, c) {}, g.prototype._pathString = function (a) {
                throw new Error("Override this function for each progress bar");
            }, g.prototype._trailString = function (a) {
                throw new Error("Override this function for each progress bar");
            }, g.prototype._warnContainerAspectRatio = function (a) {
                if (this.containerAspectRatio) {
                    var b = window.getComputedStyle(a, null),
                        c = parseFloat(b.getPropertyValue("width"), 10),
                        d = parseFloat(b.getPropertyValue("height"), 10);e.floatEquals(this.containerAspectRatio, c / d) || (console.warn("Incorrect aspect ratio of container", "#" + a.id, "detected:", b.getPropertyValue("width") + "(width)", "/", b.getPropertyValue("height") + "(height)", "=", c / d), console.warn("Aspect ratio of should be", this.containerAspectRatio));
                }
            }, b.exports = g;
        }, { "./path": 5, "./utils": 9 }], 8: [function (a, b, c) {
            var d = a("./shape"),
                e = a("./utils"),
                f = function f(a, b) {
                this._pathTemplate = "M 0,{halfOfStrokeWidth} L {width},{halfOfStrokeWidth} L {width},{width} L {halfOfStrokeWidth},{width} L {halfOfStrokeWidth},{strokeWidth}", this._trailTemplate = "M {startMargin},{halfOfStrokeWidth} L {width},{halfOfStrokeWidth} L {width},{width} L {halfOfStrokeWidth},{width} L {halfOfStrokeWidth},{halfOfStrokeWidth}", d.apply(this, arguments);
            };f.prototype = new d(), f.prototype.constructor = f, f.prototype._pathString = function (a) {
                var b = 100 - a.strokeWidth / 2;return e.render(this._pathTemplate, { width: b, strokeWidth: a.strokeWidth, halfOfStrokeWidth: a.strokeWidth / 2 });
            }, f.prototype._trailString = function (a) {
                var b = 100 - a.strokeWidth / 2;return e.render(this._trailTemplate, { width: b, strokeWidth: a.strokeWidth, halfOfStrokeWidth: a.strokeWidth / 2, startMargin: a.strokeWidth / 2 - a.trailWidth / 2 });
            }, b.exports = f;
        }, { "./shape": 7, "./utils": 9 }], 9: [function (a, b, c) {
            function d(a, b, c) {
                a = a || {}, b = b || {}, c = c || !1;for (var e in b) {
                    if (b.hasOwnProperty(e)) {
                        var f = a[e],
                            g = b[e];c && l(f) && l(g) ? a[e] = d(f, g, c) : a[e] = g;
                    }
                }return a;
            }function e(a, b) {
                var c = a;for (var d in b) {
                    if (b.hasOwnProperty(d)) {
                        var e = b[d],
                            f = "\\{" + d + "\\}",
                            g = new RegExp(f, "g");c = c.replace(g, e);
                    }
                }return c;
            }function f(a, b, c) {
                for (var d = a.style, e = 0; e < p.length; ++e) {
                    var f = p[e];d[f + h(b)] = c;
                }d[b] = c;
            }function g(a, b) {
                m(b, function (b, c) {
                    null !== b && void 0 !== b && (l(b) && b.prefix === !0 ? f(a, c, b.value) : a.style[c] = b);
                });
            }function h(a) {
                return a.charAt(0).toUpperCase() + a.slice(1);
            }function i(a) {
                return "string" == typeof a || a instanceof String;
            }function j(a) {
                return "function" == typeof a;
            }function k(a) {
                return "[object Array]" === Object.prototype.toString.call(a);
            }function l(a) {
                if (k(a)) return !1;var b = typeof a === "undefined" ? "undefined" : _typeof(a);return "object" === b && !!a;
            }function m(a, b) {
                for (var c in a) {
                    if (a.hasOwnProperty(c)) {
                        var d = a[c];b(d, c);
                    }
                }
            }function n(a, b) {
                return Math.abs(a - b) < q;
            }function o(a) {
                for (; a.firstChild;) {
                    a.removeChild(a.firstChild);
                }
            }var p = "Webkit Moz O ms".split(" "),
                q = .001;b.exports = { extend: d, render: e, setStyle: f, setStyles: g, capitalize: h, isString: i, isFunction: j, isObject: l, forEachObject: m, floatEquals: n, removeChildren: o };
        }, {}] }, {}, [4])(4);
});
//# sourceMappingURL=progressbar.min.js.map
/* jshint ignore:end */
// end file: _progressbar.js

// file: _slicknav.js
/*!
 * SlickNav Responsive Mobile Menu v1.0.10
 * (c) 2016 Josh Cope
 * licensed under MIT
 */
/* jshint ignore:start */
;(function ($, document, window) {
    var
    // default settings object.
    defaults = {
        label: 'MENU',
        duplicate: true,
        duration: 200,
        easingOpen: 'swing',
        easingClose: 'swing',
        closedSymbol: '&#9658;',
        openedSymbol: '&#9660;',
        prependTo: 'body',
        appendTo: '',
        parentTag: 'a',
        slideDirection: 'left',
        closeOnClick: false,
        allowParentLinks: false,
        nestedParentLinks: true,
        showChildren: false,
        removeIds: true,
        removeClasses: false,
        removeStyles: false,
        brand: '',
        animations: 'jquery',
        init: function init() {},
        beforeOpen: function beforeOpen() {},
        beforeClose: function beforeClose() {},
        afterOpen: function afterOpen() {},
        afterClose: function afterClose() {}
    },
        mobileMenu = 'slicknav',
        prefix = 'slicknav',
        Keyboard = {
        DOWN: 40,
        ENTER: 13,
        ESCAPE: 27,
        LEFT: 37,
        RIGHT: 39,
        SPACE: 32,
        TAB: 9,
        UP: 38
    };

    function Plugin(element, options) {
        this.element = element;

        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend({}, defaults, options);

        // Don't remove IDs by default if duplicate is false
        if (!this.settings.duplicate && !options.hasOwnProperty("removeIds")) {
            this.settings.removeIds = false;
        }

        this._defaults = defaults;
        this._name = mobileMenu;

        this.init();
    }

    Plugin.prototype.init = function () {
        var $this = this,
            menu = $(this.element),
            settings = this.settings,
            iconClass,
            menuBar;

        // clone menu if needed
        if (settings.duplicate) {
            $this.mobileNav = menu.clone();
        } else {
            $this.mobileNav = menu;
        }

        // remove IDs if set
        if (settings.removeIds) {
            $this.mobileNav.removeAttr('id');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('id');
            });
        }

        // remove classes if set
        if (settings.removeClasses) {
            $this.mobileNav.removeAttr('class');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('class');
            });
        }

        // remove styles if set
        if (settings.removeStyles) {
            $this.mobileNav.removeAttr('style');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('style');
            });
        }

        // styling class for the button
        iconClass = prefix + '_icon';

        if (settings.label === '') {
            iconClass += ' ' + prefix + '_no-text';
        }

        if (settings.parentTag == 'a') {
            settings.parentTag = 'a href="#"';
        }

        // create menu bar
        $this.mobileNav.attr('class', prefix + '_nav');
        menuBar = $('<div class="' + prefix + '_menu"></div>');
        if (settings.brand !== '') {
            var brand = $('<div class="' + prefix + '_brand">' + settings.brand + '</div>');
            $(menuBar).append(brand);
        }
        $this.btn = $(['<' + settings.parentTag + ' aria-haspopup="true" role="button" tabindex="0" class="' + prefix + '_btn ' + prefix + '_collapsed">', '<span class="' + prefix + '_menutxt">' + settings.label + '</span>', '<span class="' + iconClass + '">', '<span class="' + prefix + '_icon-bar"></span>', '<span class="' + prefix + '_icon-bar"></span>', '<span class="' + prefix + '_icon-bar"></span>', '</span>', '</' + settings.parentTag + '>'].join(''));
        $(menuBar).append($this.btn);
        if (settings.appendTo !== '') {
            $(settings.appendTo).append(menuBar);
        } else {
            $(settings.prependTo).prepend(menuBar);
        }
        menuBar.append($this.mobileNav);

        // iterate over structure adding additional structure
        var items = $this.mobileNav.find('li');
        $(items).each(function () {
            var item = $(this),
                data = {};
            data.children = item.children('ul').attr('role', 'menu');
            item.data('menu', data);

            // if a list item has a nested menu
            if (data.children.length > 0) {

                // select all text before the child menu
                // check for anchors

                var a = item.contents(),
                    containsAnchor = false,
                    nodes = [];

                $(a).each(function () {
                    if (!$(this).is('ul')) {
                        nodes.push(this);
                    } else {
                        return false;
                    }

                    if ($(this).is("a")) {
                        containsAnchor = true;
                    }
                });

                var wrapElement = $('<' + settings.parentTag + ' role="menuitem" aria-haspopup="true" tabindex="-1" class="' + prefix + '_item"/>');

                // wrap item text with tag and add classes unless we are separating parent links
                if (!settings.allowParentLinks || settings.nestedParentLinks || !containsAnchor) {
                    var $wrap = $(nodes).wrapAll(wrapElement).parent();
                    $wrap.addClass(prefix + '_row');
                } else $(nodes).wrapAll('<span class="' + prefix + '_parent-link ' + prefix + '_row"/>').parent();

                if (!settings.showChildren) {
                    item.addClass(prefix + '_collapsed');
                } else {
                    item.addClass(prefix + '_open');
                }

                item.addClass(prefix + '_parent');

                // create parent arrow. wrap with link if parent links and separating
                var arrowElement = $('<span class="' + prefix + '_arrow">' + (settings.showChildren ? settings.openedSymbol : settings.closedSymbol) + '</span>');

                if (settings.allowParentLinks && !settings.nestedParentLinks && containsAnchor) arrowElement = arrowElement.wrap(wrapElement).parent();

                //append arrow
                $(nodes).last().after(arrowElement);
            } else if (item.children().length === 0) {
                item.addClass(prefix + '_txtnode');
            }

            // accessibility for links
            item.children('a').attr('role', 'menuitem').click(function (event) {
                //Ensure that it's not a parent
                if (settings.closeOnClick && !$(event.target).parent().closest('li').hasClass(prefix + '_parent')) {
                    //Emulate menu close if set
                    $($this.btn).click();
                }
            });

            //also close on click if parent links are set
            if (settings.closeOnClick && settings.allowParentLinks) {
                item.children('a').children('a').click(function (event) {
                    //Emulate menu close
                    $($this.btn).click();
                });

                item.find('.' + prefix + '_parent-link a:not(.' + prefix + '_item)').click(function (event) {
                    //Emulate menu close
                    $($this.btn).click();
                });
            }
        });

        // structure is in place, now hide appropriate items
        $(items).each(function () {
            var data = $(this).data('menu');
            if (!settings.showChildren) {
                $this._visibilityToggle(data.children, null, false, null, true);
            }
        });

        // finally toggle entire menu
        if (settings.slideDirection === 'left') {
            settings.init();
            $this.mobileNav.stop(true, true).hide('slide', {
                direction: 'left'
            }, settings.duration);
        } else {
            $this._visibilityToggle($this.mobileNav, null, false, 'init', true);
        }

        if (settings.slideDirection === 'right') {
            settings.init();
            $this.mobileNav.stop(true, true).hide('slide', {
                direction: 'right'
            }, settings.duration);
        } else {
            $this._visibilityToggle($this.mobileNav, null, false, 'init', true);
        }

        // accessibility for menu button
        $this.mobileNav.attr('role', 'menu');

        // outline prevention when using mouse
        $(document).mousedown(function () {
            $this._outlines(false);
        });

        $(document).keyup(function () {
            $this._outlines(true);
        });

        // menu button click
        $($this.btn).click(function (e) {
            e.preventDefault();
            $this._menuToggle();
        });

        // click on menu parent
        $this.mobileNav.on('click', '.' + prefix + '_item', function (e) {
            e.preventDefault();
            $this._itemClick($(this));
        });

        // check for keyboard events on menu button and menu parents
        $($this.btn).keydown(function (e) {
            var ev = e || event;

            switch (ev.keyCode) {
                case Keyboard.ENTER:
                case Keyboard.SPACE:
                case Keyboard.DOWN:
                    e.preventDefault();
                    if (ev.keyCode !== Keyboard.DOWN || !$($this.btn).hasClass(prefix + '_open')) {
                        $this._menuToggle();
                    }

                    $($this.btn).next().find('[role="menuitem"]').first().focus();
                    break;
            }
        });

        $this.mobileNav.on('keydown', '.' + prefix + '_item', function (e) {
            var ev = e || event;

            switch (ev.keyCode) {
                case Keyboard.ENTER:
                    e.preventDefault();
                    $this._itemClick($(e.target));
                    break;
                case Keyboard.RIGHT:
                    e.preventDefault();
                    if ($(e.target).parent().hasClass(prefix + '_collapsed')) {
                        $this._itemClick($(e.target));
                    }
                    $(e.target).next().find('[role="menuitem"]').first().focus();
                    break;
            }
        });

        $this.mobileNav.on('keydown', '[role="menuitem"]', function (e) {
            var ev = e || event;

            switch (ev.keyCode) {
                case Keyboard.DOWN:
                    e.preventDefault();
                    var allItems = $(e.target).parent().parent().children().children('[role="menuitem"]:visible');
                    var idx = allItems.index(e.target);
                    var nextIdx = idx + 1;
                    if (allItems.length <= nextIdx) {
                        nextIdx = 0;
                    }
                    var next = allItems.eq(nextIdx);
                    next.focus();
                    break;
                case Keyboard.UP:
                    e.preventDefault();
                    var allItems = $(e.target).parent().parent().children().children('[role="menuitem"]:visible');
                    var idx = allItems.index(e.target);
                    var next = allItems.eq(idx - 1);
                    next.focus();
                    break;
                case Keyboard.LEFT:
                    e.preventDefault();
                    if ($(e.target).parent().parent().parent().hasClass(prefix + '_open')) {
                        var parent = $(e.target).parent().parent().prev();
                        parent.focus();
                        $this._itemClick(parent);
                    } else if ($(e.target).parent().parent().hasClass(prefix + '_nav')) {
                        $this._menuToggle();
                        $($this.btn).focus();
                    }
                    break;
                case Keyboard.ESCAPE:
                    e.preventDefault();
                    $this._menuToggle();
                    $($this.btn).focus();
                    break;
            }
        });

        // allow links clickable within parent tags if set
        if (settings.allowParentLinks && settings.nestedParentLinks) {
            $('.' + prefix + '_item a').click(function (e) {
                e.stopImmediatePropagation();
            });
        }
    };

    //toggle menu
    Plugin.prototype._menuToggle = function (el) {
        var $this = this;
        var btn = $this.btn;
        var mobileNav = $this.mobileNav;

        if (btn.hasClass(prefix + '_collapsed')) {
            btn.removeClass(prefix + '_collapsed');
            btn.addClass(prefix + '_open');
        } else {
            btn.removeClass(prefix + '_open');
            btn.addClass(prefix + '_collapsed');
        }
        btn.addClass(prefix + '_animating');
        $this._visibilityToggle(mobileNav, btn.parent(), true, btn);
    };

    // toggle clicked items
    Plugin.prototype._itemClick = function (el) {
        var $this = this;
        var settings = $this.settings;
        var data = el.data('menu');
        if (!data) {
            data = {};
            data.arrow = el.children('.' + prefix + '_arrow');
            data.ul = el.next('ul');
            data.parent = el.parent();
            //Separated parent link structure
            if (data.parent.hasClass(prefix + '_parent-link')) {
                data.parent = el.parent().parent();
                data.ul = el.parent().next('ul');
            }
            el.data('menu', data);
        }
        if (data.parent.hasClass(prefix + '_collapsed')) {
            data.arrow.html(settings.openedSymbol);
            data.parent.removeClass(prefix + '_collapsed');
            data.parent.addClass(prefix + '_open');
            data.parent.addClass(prefix + '_animating');
            $this._visibilityToggle(data.ul, data.parent, true, el);
        } else {
            data.arrow.html(settings.closedSymbol);
            data.parent.addClass(prefix + '_collapsed');
            data.parent.removeClass(prefix + '_open');
            data.parent.addClass(prefix + '_animating');
            $this._visibilityToggle(data.ul, data.parent, true, el);
        }
    };

    // toggle actual visibility and accessibility tags
    Plugin.prototype._visibilityToggle = function (el, parent, animate, trigger, init) {
        var $this = this;
        var settings = $this.settings;
        var items = $this._getActionItems(el);
        var duration = 0;
        if (animate) {
            duration = settings.duration;
        }

        function afterOpen(trigger, parent) {
            $(trigger).removeClass(prefix + '_animating');
            $(parent).removeClass(prefix + '_animating');

            //Fire afterOpen callback
            if (!init) {
                settings.afterOpen(trigger);
            }
        }

        function afterClose(trigger, parent) {
            el.attr('aria-hidden', 'true');
            items.attr('tabindex', '-1');
            $this._setVisAttr(el, true);
            el.hide(); //jQuery 1.7 bug fix

            $(trigger).removeClass(prefix + '_animating');
            $(parent).removeClass(prefix + '_animating');

            //Fire init or afterClose callback
            if (!init) {
                settings.afterClose(trigger);
            } else if (trigger == 'init') {
                settings.init();
            }
        }

        if (el.hasClass(prefix + '_hidden')) {
            el.removeClass(prefix + '_hidden');
            //Fire beforeOpen callback
            if (!init) {
                settings.beforeOpen(trigger);
            }
            if (settings.animations === 'jquery') {
                if (settings.slideDirection === 'left') {
                    if (!el.hasClass("slicknav_nav")) {
                        el.stop(true, true).slideDown(duration, settings.easingOpen, function () {
                            afterOpen(trigger, parent);
                        });
                    } else {
                        el.stop(true, true).show('slide', {
                            direction: 'left'
                        }, duration, function () {
                            afterOpen(trigger, parent);
                        });
                    }
                } else if (settings.slideDirection === 'right') {
                    if (!el.hasClass("slicknav_nav")) {
                        el.stop(true, true).slideDown(duration, settings.easingOpen, function () {
                            afterOpen(trigger, parent);
                        });
                    } else {
                        el.stop(true, true).show('slide', {
                            direction: 'right'
                        }, duration, function () {
                            afterOpen(trigger, parent);
                        });
                    }
                } else {
                    el.stop(true, true).slideDown(duration, settings.easingOpen, function () {
                        afterOpen(trigger, parent);
                    });
                }
            } else if (settings.animations === 'velocity') {
                el.velocity("finish").velocity("slideDown", {
                    duration: duration,
                    easing: settings.easingOpen,
                    complete: function complete() {
                        afterOpen(trigger, parent);
                    }
                });
            }
            el.attr('aria-hidden', 'false');
            items.attr('tabindex', '0');
            $this._setVisAttr(el, false);
        } else {
            el.addClass(prefix + '_hidden');

            //Fire init or beforeClose callback
            if (!init) {
                settings.beforeClose(trigger);
            }

            if (settings.animations === 'jquery') {
                if (settings.slideDirection === 'left') {
                    if (!el.hasClass("slicknav_nav")) {
                        el.stop(true, true).slideUp(duration, this.settings.easingClose, function () {
                            afterClose(trigger, parent);
                        });
                    } else {
                        el.stop(true, true).hide('slide', {
                            direction: 'left'
                        }, duration, function () {
                            afterClose(trigger, parent);
                        });
                    }
                } else if (settings.slideDirection === 'right') {
                    if (!el.hasClass("slicknav_nav")) {
                        el.stop(true, true).slideUp(duration, this.settings.easingClose, function () {
                            afterClose(trigger, parent);
                        });
                    } else {
                        el.stop(true, true).hide('slide', {
                            direction: 'right'
                        }, duration, function () {
                            afterClose(trigger, parent);
                        });
                    }
                } else {
                    el.stop(true, true).slideUp(duration, this.settings.easingClose, function () {
                        afterClose(trigger, parent);
                    });
                }
            } else if (settings.animations === 'velocity') {

                el.velocity("finish").velocity("slideUp", {
                    duration: duration,
                    easing: settings.easingClose,
                    complete: function complete() {
                        afterClose(trigger, parent);
                    }
                });
            }
        }
    };

    // set attributes of element and children based on visibility
    Plugin.prototype._setVisAttr = function (el, hidden) {
        var $this = this;

        // select all parents that aren't hidden
        var nonHidden = el.children('li').children('ul').not('.' + prefix + '_hidden');

        // iterate over all items setting appropriate tags
        if (!hidden) {
            nonHidden.each(function () {
                var ul = $(this);
                ul.attr('aria-hidden', 'false');
                var items = $this._getActionItems(ul);
                items.attr('tabindex', '0');
                $this._setVisAttr(ul, hidden);
            });
        } else {
            nonHidden.each(function () {
                var ul = $(this);
                ul.attr('aria-hidden', 'true');
                var items = $this._getActionItems(ul);
                items.attr('tabindex', '-1');
                $this._setVisAttr(ul, hidden);
            });
        }
    };

    // get all 1st level items that are clickable
    Plugin.prototype._getActionItems = function (el) {
        var data = el.data("menu");
        if (!data) {
            data = {};
            var items = el.children('li');
            var anchors = items.find('a');
            data.links = anchors.add(items.find('.' + prefix + '_item'));
            el.data('menu', data);
        }
        return data.links;
    };

    Plugin.prototype._outlines = function (state) {
        if (!state) {
            $('.' + prefix + '_item, .' + prefix + '_btn').css('outline', 'none');
        } else {
            $('.' + prefix + '_item, .' + prefix + '_btn').css('outline', '');
        }
    };

    Plugin.prototype.toggle = function () {
        var $this = this;
        $this._menuToggle();
    };

    Plugin.prototype.open = function () {
        var $this = this;
        if ($this.btn.hasClass(prefix + '_collapsed')) {
            $this._menuToggle();
        }
    };

    Plugin.prototype.close = function () {
        var $this = this;
        if ($this.btn.hasClass(prefix + '_open')) {
            $this._menuToggle();
        }
    };

    $.fn[mobileMenu] = function (options) {
        var args = arguments;

        // Is the first parameter an object (options), or was omitted, instantiate a new instance
        if (options === undefined || (typeof options === "undefined" ? "undefined" : _typeof(options)) === 'object') {
            return this.each(function () {

                // Only allow the plugin to be instantiated once due to methods
                if (!$.data(this, 'plugin_' + mobileMenu)) {

                    // if it has no instance, create a new one, pass options to our plugin constructor,
                    // and store the plugin instance in the elements jQuery data object.
                    $.data(this, 'plugin_' + mobileMenu, new Plugin(this, options));
                }
            });

            // If is a string and doesn't start with an underscore or 'init' function, treat this as a call to a public method.
        } else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {

            // Cache the method call to make it possible to return a value
            var returns;

            this.each(function () {
                var instance = $.data(this, 'plugin_' + mobileMenu);

                // Tests that there's already a plugin-instance and checks that the requested public method exists
                if (instance instanceof Plugin && typeof instance[options] === 'function') {

                    // Call the method of our plugin instance, and pass it the supplied arguments.
                    returns = instance[options].apply(instance, Array.prototype.slice.call(args, 1));
                }
            });

            // If the earlier cached method gives a value back return the value, otherwise return this to preserve chainability.
            return returns !== undefined ? returns : this;
        }
    };
})(jQuery, document, window);

/* jshint ignore:end */
// end file: _slicknav.js

// file: form.js
// Functions for dropdown
function select(el, currentElement) {
    $(currentElement).each(function () {
        $(this).find("select").each(function () {
            var parent = $(this).parent();
            parent.addClass('dropdown_select');
            parent.append('<div class="dropdown-header"><span>-Select-</span></div>');
            parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
            $(this).find('option').each(function () {
                if ($(this).val() != "") {
                    parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).val() + '</li>');
                }
            });
        });
    });
    $(currentElement).find('.dropdown_select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

// Input type File 

function customInputFile() {

    $('input[type=file]').parent().parent().find('label:first-child').addClass("file-name");
    $('input[type=file]').addClass("inputfile");
    $('input[type=file]').parent().parent().addClass("inputfile-fake");

    var inputs = $('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {

        var label = document.querySelectorAll('.file-name')[0];

        input.addEventListener('change', function (e) {
            var fileName = e.target.value.split('\\').pop();

            if (fileName) label.querySelector('.file-name span').innerHTML = fileName;else label.innerHTML = labelVal;
        });
    });
}

function addTrigger(i, e) {
    var triggerButton = $('<div class="show-more">Show More</div>');
    triggerButton.on('click', function () {
        var currentLi = $(this).closest('li');
        if (currentLi.hasClass('fixed-height')) {
            currentLi.removeClass('fixed-height');
            $(this).text('Show Less');
        } else {
            currentLi.addClass('fixed-height');
            $(this).text('Show More');
        }
    });
    $(e).find('.hs-form-booleancheckbox').each(function () {
        if ($(this).height() > 80) {
            $(this).addClass('fixed-height trigger');
            $(this).append(triggerButton.clone(true));
        }
    });
}
function checkForSelectChange(i, e) {
    $(e).each(function () {
        $(this).find(".dropdown-list").each(function () {
            $(this).on('click', function () {
                $(this).closest('.dropdown_select').find('.dropdown-header').addClass('selected');
            });
        });
    });
}

waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", customInputFile);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", select);
waitForLoad(".job-offers", "form", checkForSelectChange);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", addTrigger);

// end file: form.js

// file: global.js
function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}
// end file: global.js

// file: header.js
$(document).ready(function () {
    mobileMenuInit();
    manageHeaderSearch();
});

function mobileMenuInit() {
    $('.block--menu .hs-menu-wrapper > ul').slicknav({
        label: '',
        appendTo: '.block--menu',
        slideDirection: 'right',
        closedSymbol: '<i class="fa fa-angle-down" aria-hidden="true"></i>',
        openedSymbol: '<i class="fa fa-angle-up" aria-hidden="true"></i>',
        init: function init() {
            $('.slicknav_nav').addClass('preventDefault').addClass("slicknav_hidden");
        },
        beforeOpen: function beforeOpen(trigger) {
            $('.slicknav_nav').removeClass('preventDefault');
        }
    });
}

function manageHeaderSearch() {
    $('.block--search #search-btn').on('click', function (e) {
        $(e.target).closest('.input-wrapper').find('.search-popup').fadeIn(500);
        $('body').css('overflow', 'hidden');
    });
    $('.block--search .search-popup').on('click', function (e) {
        $(e.target).closest('.input-wrapper').find('.search-popup').fadeOut(500);
        e.stopPropagation();
        $('body').css('overflow', '');
    });
    $('.block--search .popup-inner').on('click', function (e) {
        e.stopPropagation();
    });
}
// end file: header.js

// file: Modules/counter-box.js
$(document).ready(function () {

    checkCounterPosition();
});

$(window).scroll(function () {

    checkCounterPosition();
});

// VARIABELS 


var checkcounter = 0;

function countTo() {

    $('.counter-box .count-to').each(function () {

        var $this = $(this);
        var countTo = $this.attr('data-count');

        $({ countNum: $this.text() }).animate({
            countNum: countTo
        }, {

            duration: 4000,
            easing: 'swing',
            step: function step() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function complete() {
                $this.text(this.countNum);
            }

        });
    });
    checkcounter = 1;
}

function checkCounterPosition() {
    var counterposition;

    if ($(".counter-box .count-to").length != 0) {
        counterposition = $(".counter-box .count-to").position().top + 100;
    }

    if ($(window).scrollTop() + $(window).height() > counterposition && checkcounter == 0) {
        countTo();
    }
}
// end file: Modules/counter-box.js

// file: Modules/rounded-bar.js
$(document).ready(function () {
    if ($('.rounded-bar--single').length > 0) {
        (function () {
            var progressBars = $('.rounded-bar--single');

            var _loop = function _loop(i) {
                var currentEl = progressBars.eq(i).find('.rounded-bar__svg');
                var currentPercentage = makePercentageVal(progressBars.eq(i).find('.rounded-bar__percentage').text());
                var bar = new ProgressBar.Circle(currentEl[0], {
                    strokeWidth: 8,
                    easing: 'linear',
                    duration: 700,
                    color: '#0098da',
                    trailColor: '#bfc4c7',
                    trailWidth: 8,
                    svgStyle: null
                });
                bar.path.style.strokeLinecap = 'round';
                progressBars.eq(i).data("bars_done", false);
                $(window).on('scroll', function () {
                    var windowHeight = $(window).height();
                    var windowScrollTop = $(window).scrollTop();
                    if (progressBars.eq(i).data("bars_done") == false) {
                        if (windowHeight + windowScrollTop > progressBars.eq(i).offset().top + 300) {
                            progressBars.eq(i).data("bars_done", true);
                            bar.animate(currentPercentage);
                        }
                    }
                });
            };

            for (var i = 0; i < progressBars.length; i++) {
                _loop(i);
            }
            $(window).scroll();
        })();
    }
});

function makePercentageVal(val) {
    val = val.slice(0, -1).trim();
    return val / 100;
}

// const ua = navigator.userAgent.toLowerCase();
// if (/msie/.test(ua) || /rv:[\d.]+\) like gecko/.test(ua)) {
//     $('body').addClass('ms-browser');
//     // var pBars = document.querySelectorAll('.rounded-bar--single svg');
//     // for (var i = 0; i < pBars.length; i++) {
//     //     pBars[i].setAttributeNS(null, 'stroke-width', '6');
//     // }
// } 
if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
    // console.log('ms browser');
    $('body').addClass('ms-browser');
}
// end file: Modules/rounded-bar.js

// file: Modules/search-jobs.js
var specsObject = undefined;
var citiesObject = {
    Łódź: 'lodz',
    Cracow: 'cracow',
    Katowice: 'katowice',
    Płock: 'plock',
    Wrocław: 'wroclaw',
    Gdynia: 'gdynia',
    Warsaw: 'warsaw'
};
$(document).ready(function () {
    var citiesArray = [];
    var specArray = [];
    suggestedCategories();
    if ($('input#offers-city').length > 0) {
        var citiesSelect = new Awesomplete('input#offers-city', {
            list: prepareJobSelects(citiesArray, '.job-offer--single__city'),
            minChars: 0,
            replace: function replace(text) {
                this.input.value = text;
                $(this.input).attr("data-value", citiesObject[text.value] || text.value);
            }
        });
        var specSelect = new Awesomplete('input#offers-specialization', {
            list: prepareSpec(specArray, '.job-offer--single__spec'),
            minChars: 0,
            replace: function replace(text) {
                this.input.value = text;
                $(this.input).attr("data-value", specsObject[text.value] || text.value);
            }
        });
        Awesomplete.$(".offers-city-button").addEventListener("click", function () {
            managePopup(citiesSelect);
        });
        Awesomplete.$(".offers-specialization-button").addEventListener("click", function () {
            managePopup(specSelect);
        });
        $('.job-offers--result').isotope();
        isotopeFiltering();
    }
});

function isotopeFiltering() {
    var req = 0;

    $('.search-bar--wrapper .input-hybrid ul').on('click', function () {
        $(this).closest('.awesomplete').find('input').trigger('change');
    });
    $('.suggested-categories li').on('click', function () {
        $('#search-pos').trigger('change');
    });
    $('.search-bar--wrapper input').on('change', function () {
        var test = [];
        $("#search-pos").val().length > 0 == true ? console.log('tak') : console.log('nie');
        if ($("#search-pos").val().length > 0) {
            req++;
            test.push("#search-pos");
        }
        if ($("#offers-city").val().length > 0) {
            req++;
            test.push("#offers-city");
        }
        if ($("#offers-specialization").val().length > 0) {
            req++;
            test.push("#offers-specialization");
        }
        $('.job-offers--result').isotope({
            filter: function filter() {
                if (req == 0) return true;
                if ($(this).attr("href") == "#") return false;
                var local_req = req;

                for (var i = 0; i < test.length; i++) {
                    var words = "";
                    if ($(test[i]).attr("data-value") == undefined) words = $(test[i]).val().split(',');else words = $(test[i]).attr("data-value").split(',');

                    for (var j = 0; j < words.length; j++) {
                        var data_attr = test[i].replace("#", '');
                        if ($(this).data(data_attr).search(words[j].trim().toLowerCase()) > -1) {
                            local_req--;
                            break;
                        }
                    }
                }
                if (local_req == 0) return true;else return false;
            }
        });
        req = 0;
    });
}

function suggestedCategories() {
    $('.suggested-categories li').on('click', function (e) {
        $('#search-pos').val($(e.currentTarget).text());
    });
}
function prepareJobSelects(arr, target) {
    var cities = document.querySelectorAll(target);
    for (var index = 0; index < cities.length; index++) {
        if (arr.indexOf(cities[index].textContent) > -1) {
            continue;
        } else {
            arr.push(cities[index].textContent);
        }
    }
    return arr;
}
function prepareSpec(arr, target) {
    var cities = document.querySelectorAll(target);
    for (var index = 0; index < cities.length; index++) {
        var currentLabel = findSpecLabel(cities[index].textContent);
        if (arr.indexOf(currentLabel) > -1) {
            continue;
        } else {
            arr.push(currentLabel);
        }
    }
    return arr;
}

function managePopup(t) {
    0 === t.ul.childNodes.length ? (t.minChars = 0, t.evaluate()) : t.ul.hasAttribute("hidden") ? t.open() : t.close();
}
function findSpecLabel(label) {

    var specs = [{
        value: "software",
        label: "Software Development"
    }, {
        value: "data_management",
        label: "Business Intelligence & Data Management"
    }, {
        value: "data_science",
        label: "Data Science"
    }, {
        value: "management",
        label: "Management & Consulting"
    }, {
        value: "analysis",
        label: "Analysis"
    }, {
        value: "quality",
        label: "Quality Assurance"
    }, {
        value: "sales",
        label: "Sales & Business Development"
    }, {
        value: "it_supp_infra",
        label: "IT Support & Infrastructure"
    }, {
        value: "marketing",
        label: "PR & Marketing"
    }, {
        value: "hr_recruitment",
        label: "HR & Recruitment"
    }];
    if (specsObject == undefined) {
        specsObject = {};
        for (var i = 0; i < specs.length; i++) {
            specsObject[specs[i].label] = specs[i].value;
        }
    }
    for (var _i = 0; _i < specs.length; _i++) {
        if (specs[_i].value == label) {
            return specs[_i].label;
        }
    }
}
// end file: Modules/search-jobs.js

// file: Modules/slick-inits.js
$('.brands-slider > span').slick({
    autoplay: true,
    rows: 2,
    slidesPerRow: 3,
    autoplaySpeed: 5000,
    dots: true,
    arrows: false,
    responsive: [{
        breakpoint: 450,
        settings: {
            slidesPerRow: 2
        }
    }, {
        breakpoint: 350,
        settings: {
            slidesPerRow: 1
        }
    }]
});
$('.brands-slider-single > span').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 2,
    slidesToScroll: 2,
    dots: true,
    arrows: false
});
// end file: Modules/slick-inits.js

// file: Modules/team-member.js
$('.single-member__toggle-desc').on('click', function () {
    var parentWrapper = $(this).closest('.single-member');
    var descToggle = parentWrapper.find('.single-member__description--toggle');
    if (parentWrapper.hasClass('open')) {
        parentWrapper.removeClass('open');
        descToggle.slideUp();
    } else {
        parentWrapper.addClass('open');
        descToggle.slideDown();
    }
});
// end file: Modules/team-member.js

// file: Website/about.js
$(document).ready(function () {

    $('.about .about-us-slider-wrapper > span').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        adaptiveHeight: true
    });
});
// end file: Website/about.js

// file: Website/carsharing.js
var isSlick = '';
var check = '';
$(document).ready(function () {

    // CUSTOMERS SLIDER 

    $('.carsharing .brands-slider-wrapper > span').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        autoplay: true,
        responsive: [{
            breakpoint: 450,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    // BLOG SLIDER 


    if ($(window).innerWidth() <= 768) {
        $('.carsharing .latest-post-from-blog').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false
        });

        isSlick = 1;
    } else {
        isSlick = 0;
        check = 1;
    }
});

$(window).resize(function () {

    if ($(window).innerWidth() > 767 && check == 0 && isSlick == 1) {

        $('.carsharing .latest-post-from-blog').slick('unslick');
        check = 1;
        // console.log(check );
    }
    if ($(window).innerWidth() < 767 && check == 1) {
        $('.carsharing .latest-post-from-blog').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false
        });
        check = 0;
        isSlick = 1;
        // console.log(check);
    }
});
// end file: Website/carsharing.js

// file: Website/finance.js
$(document).ready(function () {

    if ($(window).innerWidth() < 767) {
        $('.finance .latest-post-from-blog').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false
        });
    }
});

var checkif = 0;

$(window).resize(function () {

    if ($(window).innerWidth() > 767 && checkif == 0) {
        $('.finance .latest-post-from-blog').slick('unslick');
        checkif = 1;
    }
    if ($(window).innerWidth() < 767 && checkif == 1) {
        $('.finance .latest-post-from-blog').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false
        });
        checkif = 0;
    }
});
// end file: Website/finance.js

// file: Website/home.js
$(document).ready(function () {

    $('.home-slider-wrapper > span').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false
    });

    $('.home .brand-slider > span').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        responsive: [{
            breakpoint: 450,
            settings: {
                slidesToShow: 1
            }
        }]
    });
});
// end file: Website/home.js

// file: Website/typ.js
$(document).ready(function () {

    $('.typ-slider-wrapper > span').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        adaptiveHeight: true
    });
});
// end file: Website/typ.js
//# sourceMappingURL=template.js.map
