'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// file: form.js
// Functions for dropdown
function select(el, currentElement) {
    $(currentElement).each(function () {
        $(this).find("select").each(function () {
            var parent = $(this).parent();
            parent.addClass('dropdown_select');
            parent.append('<div class="dropdown-header"><span>-Select-</span></div>');
            parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
            $(this).find('option').each(function () {
                if ($(this).val() != "") {
                    parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).val() + '</li>');
                }
            });
        });
    });
    $(currentElement).find('.dropdown_select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

// Input type File 

function customInputFile() {

    $('input[type=file]').parent().parent().find('label:first-child').addClass("file-name");
    $('input[type=file]').addClass("inputfile");
    $('input[type=file]').parent().parent().addClass("inputfile-fake");

    var inputs = $('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {

        var label = document.querySelectorAll('.file-name')[0];

        input.addEventListener('change', function (e) {
            var fileName = e.target.value.split('\\').pop();

            if (fileName) label.querySelector('.file-name span').innerHTML = fileName;else label.innerHTML = labelVal;
        });
    });
}

function addTrigger(i, e) {
    var triggerButton = $('<div class="show-more">Show More</div>');
    triggerButton.on('click', function () {
        var currentLi = $(this).closest('li');
        if (currentLi.hasClass('fixed-height')) {
            currentLi.removeClass('fixed-height');
            $(this).text('Show Less');
        } else {
            currentLi.addClass('fixed-height');
            $(this).text('Show More');
        }
    });
    $(e).find('.hs-form-booleancheckbox').each(function () {
        if ($(this).height() > 80) {
            $(this).addClass('fixed-height trigger');
            $(this).append(triggerButton.clone(true));
        }
    });
}
function checkForSelectChange(i, e) {
    $(e).each(function () {
        $(this).find(".dropdown-list").each(function () {
            $(this).on('click', function () {
                $(this).closest('.dropdown_select').find('.dropdown-header').addClass('selected');
            });
        });
    });
}

waitForLoad(".widget-type-form, .form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", customInputFile);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", select);
waitForLoad(".job-offers", "form", checkForSelectChange);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", addTrigger);

// end file: form.js

// file: global.js
function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}
// end file: global.js

// file: header.js
$(document).ready(function () {
    mobileMenuInit();
    manageHeaderSearch();
});

function mobileMenuInit() {
    $('.block--menu .hs-menu-wrapper > ul').slicknav({
        label: '',
        appendTo: '.block--menu',
        slideDirection: 'right',
        closedSymbol: '<i class="fa fa-angle-down" aria-hidden="true"></i>',
        openedSymbol: '<i class="fa fa-angle-up" aria-hidden="true"></i>',
        init: function init() {
            $('.slicknav_nav').addClass('preventDefault').addClass("slicknav_hidden");
        },
        beforeOpen: function beforeOpen(trigger) {
            $('.slicknav_nav').removeClass('preventDefault');
        }
    });
}

function manageHeaderSearch() {
    $('.block--search #search-btn').on('click', function (e) {
        $(e.target).closest('.input-wrapper').find('.search-popup').fadeIn(500);
        $('body').css('overflow', 'hidden');
    });
    $('.block--search .search-popup').on('click', function (e) {
        $(e.target).closest('.input-wrapper').find('.search-popup').fadeOut(500);
        e.stopPropagation();
        $('body').css('overflow', '');
    });
    $('.block--search .popup-inner').on('click', function (e) {
        e.stopPropagation();
    });
}
// end file: header.js

// file: _slicknav.js
/*!
 * SlickNav Responsive Mobile Menu v1.0.10
 * (c) 2016 Josh Cope
 * licensed under MIT
 */
;(function ($, document, window) {
    var
    // default settings object.
    defaults = {
        label: 'MENU',
        duplicate: true,
        duration: 200,
        easingOpen: 'swing',
        easingClose: 'swing',
        closedSymbol: '&#9658;',
        openedSymbol: '&#9660;',
        prependTo: 'body',
        appendTo: '',
        parentTag: 'a',
        slideDirection: 'left',
        closeOnClick: false,
        allowParentLinks: false,
        nestedParentLinks: true,
        showChildren: false,
        removeIds: true,
        removeClasses: false,
        removeStyles: false,
        brand: '',
        animations: 'jquery',
        init: function init() {},
        beforeOpen: function beforeOpen() {},
        beforeClose: function beforeClose() {},
        afterOpen: function afterOpen() {},
        afterClose: function afterClose() {}
    },
        mobileMenu = 'slicknav',
        prefix = 'slicknav',
        Keyboard = {
        DOWN: 40,
        ENTER: 13,
        ESCAPE: 27,
        LEFT: 37,
        RIGHT: 39,
        SPACE: 32,
        TAB: 9,
        UP: 38
    };

    function Plugin(element, options) {
        this.element = element;

        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend({}, defaults, options);

        // Don't remove IDs by default if duplicate is false
        if (!this.settings.duplicate && !options.hasOwnProperty("removeIds")) {
            this.settings.removeIds = false;
        }

        this._defaults = defaults;
        this._name = mobileMenu;

        this.init();
    }

    Plugin.prototype.init = function () {
        var $this = this,
            menu = $(this.element),
            settings = this.settings,
            iconClass,
            menuBar;

        // clone menu if needed
        if (settings.duplicate) {
            $this.mobileNav = menu.clone();
        } else {
            $this.mobileNav = menu;
        }

        // remove IDs if set
        if (settings.removeIds) {
            $this.mobileNav.removeAttr('id');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('id');
            });
        }

        // remove classes if set
        if (settings.removeClasses) {
            $this.mobileNav.removeAttr('class');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('class');
            });
        }

        // remove styles if set
        if (settings.removeStyles) {
            $this.mobileNav.removeAttr('style');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('style');
            });
        }

        // styling class for the button
        iconClass = prefix + '_icon';

        if (settings.label === '') {
            iconClass += ' ' + prefix + '_no-text';
        }

        if (settings.parentTag == 'a') {
            settings.parentTag = 'a href="#"';
        }

        // create menu bar
        $this.mobileNav.attr('class', prefix + '_nav');
        menuBar = $('<div class="' + prefix + '_menu"></div>');
        if (settings.brand !== '') {
            var brand = $('<div class="' + prefix + '_brand">' + settings.brand + '</div>');
            $(menuBar).append(brand);
        }
        $this.btn = $(['<' + settings.parentTag + ' aria-haspopup="true" role="button" tabindex="0" class="' + prefix + '_btn ' + prefix + '_collapsed">', '<span class="' + prefix + '_menutxt">' + settings.label + '</span>', '<span class="' + iconClass + '">', '<span class="' + prefix + '_icon-bar"></span>', '<span class="' + prefix + '_icon-bar"></span>', '<span class="' + prefix + '_icon-bar"></span>', '</span>', '</' + settings.parentTag + '>'].join(''));
        $(menuBar).append($this.btn);
        if (settings.appendTo !== '') {
            $(settings.appendTo).append(menuBar);
        } else {
            $(settings.prependTo).prepend(menuBar);
        }
        menuBar.append($this.mobileNav);

        // iterate over structure adding additional structure
        var items = $this.mobileNav.find('li');
        $(items).each(function () {
            var item = $(this),
                data = {};
            data.children = item.children('ul').attr('role', 'menu');
            item.data('menu', data);

            // if a list item has a nested menu
            if (data.children.length > 0) {

                // select all text before the child menu
                // check for anchors

                var a = item.contents(),
                    containsAnchor = false,
                    nodes = [];

                $(a).each(function () {
                    if (!$(this).is('ul')) {
                        nodes.push(this);
                    } else {
                        return false;
                    }

                    if ($(this).is("a")) {
                        containsAnchor = true;
                    }
                });

                var wrapElement = $('<' + settings.parentTag + ' role="menuitem" aria-haspopup="true" tabindex="-1" class="' + prefix + '_item"/>');

                // wrap item text with tag and add classes unless we are separating parent links
                if (!settings.allowParentLinks || settings.nestedParentLinks || !containsAnchor) {
                    var $wrap = $(nodes).wrapAll(wrapElement).parent();
                    $wrap.addClass(prefix + '_row');
                } else $(nodes).wrapAll('<span class="' + prefix + '_parent-link ' + prefix + '_row"/>').parent();

                if (!settings.showChildren) {
                    item.addClass(prefix + '_collapsed');
                } else {
                    item.addClass(prefix + '_open');
                }

                item.addClass(prefix + '_parent');

                // create parent arrow. wrap with link if parent links and separating
                var arrowElement = $('<span class="' + prefix + '_arrow">' + (settings.showChildren ? settings.openedSymbol : settings.closedSymbol) + '</span>');

                if (settings.allowParentLinks && !settings.nestedParentLinks && containsAnchor) arrowElement = arrowElement.wrap(wrapElement).parent();

                //append arrow
                $(nodes).last().after(arrowElement);
            } else if (item.children().length === 0) {
                item.addClass(prefix + '_txtnode');
            }

            // accessibility for links
            item.children('a').attr('role', 'menuitem').click(function (event) {
                //Ensure that it's not a parent
                if (settings.closeOnClick && !$(event.target).parent().closest('li').hasClass(prefix + '_parent')) {
                    //Emulate menu close if set
                    $($this.btn).click();
                }
            });

            //also close on click if parent links are set
            if (settings.closeOnClick && settings.allowParentLinks) {
                item.children('a').children('a').click(function (event) {
                    //Emulate menu close
                    $($this.btn).click();
                });

                item.find('.' + prefix + '_parent-link a:not(.' + prefix + '_item)').click(function (event) {
                    //Emulate menu close
                    $($this.btn).click();
                });
            }
        });

        // structure is in place, now hide appropriate items
        $(items).each(function () {
            var data = $(this).data('menu');
            if (!settings.showChildren) {
                $this._visibilityToggle(data.children, null, false, null, true);
            }
        });

        // finally toggle entire menu
        if (settings.slideDirection === 'left') {
            settings.init();
            $this.mobileNav.stop(true, true).hide('slide', {
                direction: 'left'
            }, settings.duration);
        } else {
            $this._visibilityToggle($this.mobileNav, null, false, 'init', true);
        }

        if (settings.slideDirection === 'right') {
            settings.init();
            $this.mobileNav.stop(true, true).hide('slide', {
                direction: 'right'
            }, settings.duration);
        } else {
            $this._visibilityToggle($this.mobileNav, null, false, 'init', true);
        }

        // accessibility for menu button
        $this.mobileNav.attr('role', 'menu');

        // outline prevention when using mouse
        $(document).mousedown(function () {
            $this._outlines(false);
        });

        $(document).keyup(function () {
            $this._outlines(true);
        });

        // menu button click
        $($this.btn).click(function (e) {
            e.preventDefault();
            $this._menuToggle();
        });

        // click on menu parent
        $this.mobileNav.on('click', '.' + prefix + '_item', function (e) {
            e.preventDefault();
            $this._itemClick($(this));
        });

        // check for keyboard events on menu button and menu parents
        $($this.btn).keydown(function (e) {
            var ev = e || event;

            switch (ev.keyCode) {
                case Keyboard.ENTER:
                case Keyboard.SPACE:
                case Keyboard.DOWN:
                    e.preventDefault();
                    if (ev.keyCode !== Keyboard.DOWN || !$($this.btn).hasClass(prefix + '_open')) {
                        $this._menuToggle();
                    }

                    $($this.btn).next().find('[role="menuitem"]').first().focus();
                    break;
            }
        });

        $this.mobileNav.on('keydown', '.' + prefix + '_item', function (e) {
            var ev = e || event;

            switch (ev.keyCode) {
                case Keyboard.ENTER:
                    e.preventDefault();
                    $this._itemClick($(e.target));
                    break;
                case Keyboard.RIGHT:
                    e.preventDefault();
                    if ($(e.target).parent().hasClass(prefix + '_collapsed')) {
                        $this._itemClick($(e.target));
                    }
                    $(e.target).next().find('[role="menuitem"]').first().focus();
                    break;
            }
        });

        $this.mobileNav.on('keydown', '[role="menuitem"]', function (e) {
            var ev = e || event;

            switch (ev.keyCode) {
                case Keyboard.DOWN:
                    e.preventDefault();
                    var allItems = $(e.target).parent().parent().children().children('[role="menuitem"]:visible');
                    var idx = allItems.index(e.target);
                    var nextIdx = idx + 1;
                    if (allItems.length <= nextIdx) {
                        nextIdx = 0;
                    }
                    var next = allItems.eq(nextIdx);
                    next.focus();
                    break;
                case Keyboard.UP:
                    e.preventDefault();
                    var allItems = $(e.target).parent().parent().children().children('[role="menuitem"]:visible');
                    var idx = allItems.index(e.target);
                    var next = allItems.eq(idx - 1);
                    next.focus();
                    break;
                case Keyboard.LEFT:
                    e.preventDefault();
                    if ($(e.target).parent().parent().parent().hasClass(prefix + '_open')) {
                        var parent = $(e.target).parent().parent().prev();
                        parent.focus();
                        $this._itemClick(parent);
                    } else if ($(e.target).parent().parent().hasClass(prefix + '_nav')) {
                        $this._menuToggle();
                        $($this.btn).focus();
                    }
                    break;
                case Keyboard.ESCAPE:
                    e.preventDefault();
                    $this._menuToggle();
                    $($this.btn).focus();
                    break;
            }
        });

        // allow links clickable within parent tags if set
        if (settings.allowParentLinks && settings.nestedParentLinks) {
            $('.' + prefix + '_item a').click(function (e) {
                e.stopImmediatePropagation();
            });
        }
    };

    //toggle menu
    Plugin.prototype._menuToggle = function (el) {
        var $this = this;
        var btn = $this.btn;
        var mobileNav = $this.mobileNav;

        if (btn.hasClass(prefix + '_collapsed')) {
            btn.removeClass(prefix + '_collapsed');
            btn.addClass(prefix + '_open');
        } else {
            btn.removeClass(prefix + '_open');
            btn.addClass(prefix + '_collapsed');
        }
        btn.addClass(prefix + '_animating');
        $this._visibilityToggle(mobileNav, btn.parent(), true, btn);
    };

    // toggle clicked items
    Plugin.prototype._itemClick = function (el) {
        var $this = this;
        var settings = $this.settings;
        var data = el.data('menu');
        if (!data) {
            data = {};
            data.arrow = el.children('.' + prefix + '_arrow');
            data.ul = el.next('ul');
            data.parent = el.parent();
            //Separated parent link structure
            if (data.parent.hasClass(prefix + '_parent-link')) {
                data.parent = el.parent().parent();
                data.ul = el.parent().next('ul');
            }
            el.data('menu', data);
        }
        if (data.parent.hasClass(prefix + '_collapsed')) {
            data.arrow.html(settings.openedSymbol);
            data.parent.removeClass(prefix + '_collapsed');
            data.parent.addClass(prefix + '_open');
            data.parent.addClass(prefix + '_animating');
            $this._visibilityToggle(data.ul, data.parent, true, el);
        } else {
            data.arrow.html(settings.closedSymbol);
            data.parent.addClass(prefix + '_collapsed');
            data.parent.removeClass(prefix + '_open');
            data.parent.addClass(prefix + '_animating');
            $this._visibilityToggle(data.ul, data.parent, true, el);
        }
    };

    // toggle actual visibility and accessibility tags
    Plugin.prototype._visibilityToggle = function (el, parent, animate, trigger, init) {
        var $this = this;
        var settings = $this.settings;
        var items = $this._getActionItems(el);
        var duration = 0;
        if (animate) {
            duration = settings.duration;
        }

        function afterOpen(trigger, parent) {
            $(trigger).removeClass(prefix + '_animating');
            $(parent).removeClass(prefix + '_animating');

            //Fire afterOpen callback
            if (!init) {
                settings.afterOpen(trigger);
            }
        }

        function afterClose(trigger, parent) {
            el.attr('aria-hidden', 'true');
            items.attr('tabindex', '-1');
            $this._setVisAttr(el, true);
            el.hide(); //jQuery 1.7 bug fix

            $(trigger).removeClass(prefix + '_animating');
            $(parent).removeClass(prefix + '_animating');

            //Fire init or afterClose callback
            if (!init) {
                settings.afterClose(trigger);
            } else if (trigger == 'init') {
                settings.init();
            }
        }

        if (el.hasClass(prefix + '_hidden')) {
            el.removeClass(prefix + '_hidden');
            //Fire beforeOpen callback
            if (!init) {
                settings.beforeOpen(trigger);
            }
            if (settings.animations === 'jquery') {
                if (settings.slideDirection === 'left') {
                    if (!el.hasClass("slicknav_nav")) {
                        el.stop(true, true).slideDown(duration, settings.easingOpen, function () {
                            afterOpen(trigger, parent);
                        });
                    } else {
                        el.stop(true, true).show('slide', {
                            direction: 'left'
                        }, duration, function () {
                            afterOpen(trigger, parent);
                        });
                    }
                } else if (settings.slideDirection === 'right') {
                    if (!el.hasClass("slicknav_nav")) {
                        el.stop(true, true).slideDown(duration, settings.easingOpen, function () {
                            afterOpen(trigger, parent);
                        });
                    } else {
                        el.stop(true, true).show('slide', {
                            direction: 'right'
                        }, duration, function () {
                            afterOpen(trigger, parent);
                        });
                    }
                } else {
                    el.stop(true, true).slideDown(duration, settings.easingOpen, function () {
                        afterOpen(trigger, parent);
                    });
                }
            } else if (settings.animations === 'velocity') {
                el.velocity("finish").velocity("slideDown", {
                    duration: duration,
                    easing: settings.easingOpen,
                    complete: function complete() {
                        afterOpen(trigger, parent);
                    }
                });
            }
            el.attr('aria-hidden', 'false');
            items.attr('tabindex', '0');
            $this._setVisAttr(el, false);
        } else {
            el.addClass(prefix + '_hidden');

            //Fire init or beforeClose callback
            if (!init) {
                settings.beforeClose(trigger);
            }

            if (settings.animations === 'jquery') {
                if (settings.slideDirection === 'left') {
                    if (!el.hasClass("slicknav_nav")) {
                        el.stop(true, true).slideUp(duration, this.settings.easingClose, function () {
                            afterClose(trigger, parent);
                        });
                    } else {
                        el.stop(true, true).hide('slide', {
                            direction: 'left'
                        }, duration, function () {
                            afterClose(trigger, parent);
                        });
                    }
                } else if (settings.slideDirection === 'right') {
                    if (!el.hasClass("slicknav_nav")) {
                        el.stop(true, true).slideUp(duration, this.settings.easingClose, function () {
                            afterClose(trigger, parent);
                        });
                    } else {
                        el.stop(true, true).hide('slide', {
                            direction: 'right'
                        }, duration, function () {
                            afterClose(trigger, parent);
                        });
                    }
                } else {
                    el.stop(true, true).slideUp(duration, this.settings.easingClose, function () {
                        afterClose(trigger, parent);
                    });
                }
            } else if (settings.animations === 'velocity') {

                el.velocity("finish").velocity("slideUp", {
                    duration: duration,
                    easing: settings.easingClose,
                    complete: function complete() {
                        afterClose(trigger, parent);
                    }
                });
            }
        }
    };

    // set attributes of element and children based on visibility
    Plugin.prototype._setVisAttr = function (el, hidden) {
        var $this = this;

        // select all parents that aren't hidden
        var nonHidden = el.children('li').children('ul').not('.' + prefix + '_hidden');

        // iterate over all items setting appropriate tags
        if (!hidden) {
            nonHidden.each(function () {
                var ul = $(this);
                ul.attr('aria-hidden', 'false');
                var items = $this._getActionItems(ul);
                items.attr('tabindex', '0');
                $this._setVisAttr(ul, hidden);
            });
        } else {
            nonHidden.each(function () {
                var ul = $(this);
                ul.attr('aria-hidden', 'true');
                var items = $this._getActionItems(ul);
                items.attr('tabindex', '-1');
                $this._setVisAttr(ul, hidden);
            });
        }
    };

    // get all 1st level items that are clickable
    Plugin.prototype._getActionItems = function (el) {
        var data = el.data("menu");
        if (!data) {
            data = {};
            var items = el.children('li');
            var anchors = items.find('a');
            data.links = anchors.add(items.find('.' + prefix + '_item'));
            el.data('menu', data);
        }
        return data.links;
    };

    Plugin.prototype._outlines = function (state) {
        if (!state) {
            $('.' + prefix + '_item, .' + prefix + '_btn').css('outline', 'none');
        } else {
            $('.' + prefix + '_item, .' + prefix + '_btn').css('outline', '');
        }
    };

    Plugin.prototype.toggle = function () {
        var $this = this;
        $this._menuToggle();
    };

    Plugin.prototype.open = function () {
        var $this = this;
        if ($this.btn.hasClass(prefix + '_collapsed')) {
            $this._menuToggle();
        }
    };

    Plugin.prototype.close = function () {
        var $this = this;
        if ($this.btn.hasClass(prefix + '_open')) {
            $this._menuToggle();
        }
    };

    $.fn[mobileMenu] = function (options) {
        var args = arguments;

        // Is the first parameter an object (options), or was omitted, instantiate a new instance
        if (options === undefined || (typeof options === 'undefined' ? 'undefined' : _typeof(options)) === 'object') {
            return this.each(function () {

                // Only allow the plugin to be instantiated once due to methods
                if (!$.data(this, 'plugin_' + mobileMenu)) {

                    // if it has no instance, create a new one, pass options to our plugin constructor,
                    // and store the plugin instance in the elements jQuery data object.
                    $.data(this, 'plugin_' + mobileMenu, new Plugin(this, options));
                }
            });

            // If is a string and doesn't start with an underscore or 'init' function, treat this as a call to a public method.
        } else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {

            // Cache the method call to make it possible to return a value
            var returns;

            this.each(function () {
                var instance = $.data(this, 'plugin_' + mobileMenu);

                // Tests that there's already a plugin-instance and checks that the requested public method exists
                if (instance instanceof Plugin && typeof instance[options] === 'function') {

                    // Call the method of our plugin instance, and pass it the supplied arguments.
                    returns = instance[options].apply(instance, Array.prototype.slice.call(args, 1));
                }
            });

            // If the earlier cached method gives a value back return the value, otherwise return this to preserve chainability.
            return returns !== undefined ? returns : this;
        }
    };
})(jQuery, document, window);
// end file: _slicknav.js

// file: Modules\counter-box.js
$(document).ready(function () {

    checkCounterPosition();
});

$(window).scroll(function () {

    checkCounterPosition();
});

// VARIABELS 


var checkcounter = 0;

function countTo() {

    $('.counter-box .count-to').each(function () {

        var $this = $(this);
        var countTo = $this.attr('data-count');

        $({ countNum: $this.text() }).animate({
            countNum: countTo
        }, {

            duration: 4000,
            easing: 'swing',
            step: function step() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function complete() {
                $this.text(this.countNum);
            }

        });
    });
    checkcounter = 1;
}

function checkCounterPosition() {
    var counterposition;

    if ($(".counter-box .count-to").length != 0) {
        counterposition = $(".counter-box .count-to").position().top + 100;
    }

    if ($(window).scrollTop() + $(window).height() > counterposition && checkcounter == 0) {
        countTo();
    }
}
// end file: Modules\counter-box.js

// file: Modules\slick-inits.js
$('.brands-slider > span').slick({
    autoplay: true,
    rows: 2,
    slidesPerRow: 3,
    autoplaySpeed: 5000,
    dots: true,
    arrows: false
});
// end file: Modules\slick-inits.js

// file: Modules\team-member.js
$('.single-member__toggle-desc').on('click', function () {
    var parentWrapper = $(this).closest('.single-member');
    var descToggle = parentWrapper.find('.single-member__description--toggle');
    if (parentWrapper.hasClass('open')) {
        parentWrapper.removeClass('open');
        descToggle.slideUp();
    } else {
        parentWrapper.addClass('open');
        descToggle.slideDown();
    }
});
// end file: Modules\team-member.js

// file: Website\about.js
$(document).ready(function () {

    $('.about .about-us-slider-wrapper > span').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        adaptiveHeight: true
    });
});
// end file: Website\about.js

// file: Website\home.js
$(document).ready(function () {

    $('.home-slider-wrapper > span').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false
    });

    $('.brand-slider > span').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        responsive: [{
            breakpoint: 450,
            settings: {
                slidesToShow: 1
            }
        }]
    });
});
// end file: Website\home.js

// file: Website\typ.js
$(document).ready(function () {

    $('.typ-slider-wrapper > span').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        adaptiveHeight: true
    });
});
// end file: Website\typ.js
//# sourceMappingURL=template.js.map
